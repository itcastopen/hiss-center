package cn.itcast.hiss.sdk.web.oa.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/*
 * @author miukoo
 * @description 自动装配OA相关接口
 * @date 2023/7/12 10:48
 * @version 1.0
 **/
@ComponentScan("cn.itcast.hiss.sdk.web.oa")
public class HissWepApiOaAutoConfiguration {



}
