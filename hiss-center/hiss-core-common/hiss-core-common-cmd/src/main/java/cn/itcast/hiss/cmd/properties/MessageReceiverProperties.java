package cn.itcast.hiss.cmd.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/*
 * @author miukoo
 * @description //TODO
 * @date 2023/5/12 23:55
 * @version 1.0
 **/
@ConfigurationProperties(prefix = "hiss.message.receiver")
public class MessageReceiverProperties {
}
