package cn.itcast.hiss.api.client.processdefinition;

import lombok.Data;

/**
 * DeloymentInfo
 *
 * @author: wgl
 * @describe: 流程定义基础信息
 * @date: 2022/12/28 10:10
 */
@Data
public class DeloymentInfo {

    private String id;

    private String name;
}
