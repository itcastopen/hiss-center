package cn.itcast.hiss.common.enums;

/**
 * InputType
 * 输入类型
 */
public enum InputType {

    TREEQUERY,
    SELECTQUERY,
    INPUT,
    NONE,
}
