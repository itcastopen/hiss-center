package cn.itcast.hiss.message;

/*
 * @author miukoo
 * @description 支持的消息通道类型
 * @date 2023/5/12 21:42
 * @version 1.0
 **/
public enum MessageChannelType {

    NETTY,HTTP,KAFKA;

}
