package cn.itcast.hiss.process.activiti.service;

/*
 * @author miukoo
 * @description 流程分类管理
 * @date 2023/6/25 8:51
 * @version 1.0
 **/
public interface HissProcessCategoryService {

    public void insertDefaultCategory(String userAppId);
}
