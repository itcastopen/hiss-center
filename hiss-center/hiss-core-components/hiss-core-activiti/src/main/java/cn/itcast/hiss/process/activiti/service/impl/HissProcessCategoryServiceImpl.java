package cn.itcast.hiss.process.activiti.service.impl;

import cn.itcast.hiss.process.activiti.service.HissProcessCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 * @author miukoo
 * @description 分类管理
 * @date 2023/6/25 8:52
 * @version 1.0
 **/
@Service
@Transactional
public class HissProcessCategoryServiceImpl implements HissProcessCategoryService {

    private static final String DEFAULT_CATEGORY[] = {""};

    @Override
    public void insertDefaultCategory(String userAppId) {

    }
}
