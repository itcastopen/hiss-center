# 流程中心

#### 项目简介

> 流程中心主要是想审批流程的设计及相关自定义表单的设计

项目迭代日志：
23 年 6 月 7 - 提交初始代码

#### 研发规范遵循：

- 主体使用驼峰命名
- 公共样式使用 - 连接命名
- 内部样式 驼峰命名
- 页面命名 使用小写开头的驼峰命名
- 组件使用大写开头命名

#### 产品原型及设计

#### 运行环境 - 初始开发环境及工具

- 项目开发环境: Mac + node: v17.8.0 + npm: 8.12.1 || pnpm: 6.32.8

#### 访问地址

#### 技术栈应用

Vue 3 + TypeScript +Tdesign + Vite + pinia

#### 项目结构

```

├── commitlint.config.js - commintlint 规范
├── docker - docker 部署配置文件
│
└── nginx.conf - 若开启 browerhistroy 可参考配置
├── docs - 项目展示图 -首页截图
├── globals.d.ts - 通用声明文件
├── index.html - 主 html 文件
├── mock - mock目录
│ └── index.ts
├── node_modules - 项目依赖
├── package-lock.json
├── package.json
├── public - 公用资源文件
│ └── favicon.ico
├── shims-vue.d.ts
├──src - 页面代码
│ ├── api 请求相关
│ ├── assets 公共资源
│ │ ├── images 图片资源
│ ├── api - 接口
│ ├── conponents - 公用组件
│ │ ├── Delete - 删除弹层：只需从父组件传删除的内容提示
│ │ ├── ImageMagnify - 查看图片弹层
│ │ ├── Message - 提示弹层
│ │ │ ├──Success - 成功通知弹窗
│ │ │ ├──ProdDisabled - 禁用提示弹窗
│ │ ├── switchBar - tab切换
│ │ │ ├──switchBar - tab切换
│ │ │ ├──switchBarindex - 首页tab切换
│ │ │ ├──switchBartop - 线条tab
│ ├── czriComponents - 组件包：常用，但是原有组件库实现不了，因此进行了二次开发
│ │ ├──AddInput - 动态添加input组件
│ │ ├──CardListCollapse - 折叠列表
│ │ ├── CardListSort - 带排序的列表
│ │ ├──dropList - 可下拉展开的列表
│ │ ├──ListDialog - 常用弹窗5（带列表的弹窗）
│ │ ├──ListScrollDialog - 常用弹窗5（带列表的弹窗、滚动分页）
│ │ ├──TabDialog - 常用弹窗6（带tab的弹窗）
│ │ ├──tabList - 带tab的列表
│ │ ├──Transfer - 穿梭框(标题有hover状态)
│ │ ├──treeList - 树形列表
│ │ ├──UnitDialog - 常用弹窗2（带单位和数显）
│ │ ├──index.less - 组件包样式
│ ├── layouts - 页面架构
│ │ ├──components - 页面架构公共组件
│ │ │ ├──Breadcrumb - 面包屑
│ │ │ ├──Content - 内置组件，避免重复渲染DOM
│ │ │ ├──Footer - 底部公司名称
│ │ │ ├──LayoutContentSide - 侧边栏
│ │ │ ├──LayoutHeader - 侧边栏头部
│ │ │ ├──Loginfo - 侧边栏退出区域
│ │ │ ├──Notice - 通知中心，弃用
│ │ │ ├──Search - 搜索功能
│ │ ├──frame - 页面架构框架
│ │ ├──simple2Components - 框架二公用内容
│ │ │ ├──Header - 框架顶部
│ │ ├──simpleComponents - 框架公用内容
│ │ │ ├──MenuContent - 简版布局 
│ │ │ ├──SideNav - 列表菜单 
│ │──index.vue - 框架布局
│ │──setting.vue = 设置框架风格 
│ ├── pages - 页面展示目录 
│ │ ├──dashboard - 首页 
│ │ ├──detail - 详情页 
│ │ │ ├──advanced - 多卡片详情页 
│ │ │ ├──base - 基础详情页 
│ │ │ ├──deploy - 数据详情页 
│ │ │ ├──secondary - 二级详情页 
│ │ ├──form - 表单页 
│ │ │ ├──base - 基础表单页 
│ │ │ ├──step - 分步表单页 
│ │ ├──login - 普通tab登录页 
│ │ ├──login2 - 左右布局登录页 
│ │ ├──module - 组件包调用入口页，调用的组件都在(czriComponents文件加中) 
│ │ ├──user - 个人中心
│ │ ├──list - 列表页 
│ │ │ ├──base - 基础列表页 
│ │ │ ├──upBase - 基础列表页（带图） 
│ │ │ ├──noSearch - 基础列表页（不带搜索） 
│ │ │ ├──card - 卡片列表页 
│ │ │ ├──noData - 列表无数据页 
│ │ │ ├──statistics - 统计数据列表页 
│ │ │ ├──tab - tab切换 
│ │ │ ├──topTab - 标签列表页（顶部） 
│ │ │ ├──tree - 树状筛选列表页 
│ ├── router - 定义路由页面 
│ ├── style - 样式 
│ │ ├──componentsReast - 组件重置、全局样式 
│ │ ├──theme - 全局颜色值、公用样式 
│ │ index.less - 样式总入口 
│ │ normal.less - 普通框架样式 
│ │ noSecondMenu.less - 普通框架简化版样式 
│ │ top.less - 上左右布局 
│ ├── utils 封装工具目录 
│ ├── main.ts - 项目入口文件 
│ ├── permission.ts - 路有权限控制 
├── tsconfig.json - ts配置文件 
├── README.md - 说明文档 
└── vite.config.js - vite 配置文件

```

#### 安装运行

```bash
## 安装依赖
npm install || yarn

## 启动项目

# 启动链接mock
npm run dev
# 启动链接测试环境
npm run start

## 构建正式环境 - 打包
npm run build

```

#### 插件

nprogress 进度条

viteMockServe vite 的数据模拟插件

vueJsx

> 使用 jsx 语法 jsx 语法可以更好地跟 Typescript 结合 在阅读 UI 框架源码时，发现在知名 UI 组件库 Ant Design Vue 跟 Naive UI 皆使用 tsx 语法开发
> vite-svg-loader

#### 参考

vite
vue3
pinia 中文文档 :类 vuex
vue-router
Tdesign
Tdesign-cli

tsconfig.json 配置

#### 文档

tsconfig.json 配置整理
vite.config.js vite 配置文件


# 常见环境问题
process-designer-ui部署前需要修改package.json，如果在编译properties-panel的时候还有可能需要配置国内镜像下载puppeteer，默认弹出的页面上也没有任何提示开发人员是需要访问route里的特定路径才会显示页面，对我这种不会前端的年轻后端不太友好。

 **解决方案** 
基于我的解决过程提供一个简单的markdown文本，但是由于我不是很懂前端，无法保证内容的可靠性，希望能帮助到您完善Readme文件

## 使用指南

### 确认项目路径

1. 确保 `process-designer-ui` 和 `properties-panel` 项目位于同一级目录中。

### 打包`properties-panel`

2. **不要** 直接去`process-designer-ui` 目录下运行 `npm install`。

3. 切换到 `properties-panel` 目录，并运行 `yarn` 命令（也可以尝试使用 `npm install`需要配置国内镜像）

   ```bash
   cd properties-panel
   yarn
   ```

   - **注意**：如果 `yarn` 命令没有自动执行 build 脚本，请参考 `package.json` 文件中的内容手动执行：

     ```json
     "scripts": {
       "build": "del-cli preact dist && rollup -c",
       // ... 其他脚本
     }
     ```

4. 打包完成后不需要修改任何其他内容

### 部署`process-designer-ui`

5. 打包完成后，返回到 `process-designer-ui` 目录，并编辑 `package.json` 和 `package-lock.json` 文件

   - 将 `@bpmn-io/properties-panel` 的路径更改为相对路径：

     ```json
     "@bpmn-io/properties-panel": "../properties-panel"
     ```

6. 在 `process-designer-ui` 目录下运行 `yarn` 来安装依赖。

   ```bash
   yarn
   ```

### 访问页面

7. 打开`process-designer-ui`项目内的 `process-designer-ui/src/router/index.js` 文件，查看定义的路由路径。
8. 在浏览器中打开 `http://localhost:8080/#/`。这是一个空白页面，您需要在 URL 后添加在 `index.js` 中定义的相应路径来访问特定页面。例如：`http://localhost:8080/#/bis`。