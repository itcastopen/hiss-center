export const COLUMNS = [
  {
    title: '编号',
    align: 'left',
    width: 160,
    minWidth: 100,
    colKey: 'index'
  },
  {
    title: '表单名称',
    colKey: 'name',
    width: 300,
    cell: { col: 'status' }
  },
  {
    title: 'icon',
    minWidth: 150,
    colKey: 'icon'
  },
  {
    title: 'color',
    minWidth: 150,
    colKey: 'color'
  },
  {
    title: '表单描述',
    minWidth: 150,
    colKey: 'formDescribe'
  },
  {
    align: 'left',
    fixed: 'right',
    width: 157,
    minWidth: '157px',
    colKey: 'op',
    title: '操作'
  }
]
