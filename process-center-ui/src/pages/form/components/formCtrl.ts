// 控件源数据
const ctrlDataes = [
  {
    type: 'title',
    lab: '基础控件'
  },
  {
    type: 'input',
    lab: '单行输入框',
    icon: 'icon_danhang2x',
    data: {
      title: '单行输入框',
      placeholder: '请输入',
      width: 100,
      defaultWidth: '1', //  默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      minLength: '',
      maxLength: '',
      isReadOnly: false, // 只读
      isStatistics: false, // 输入统计
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isMust: false, // 是否必填
      isOnly: false, // 唯一
      inputType: '', // 反馈类型 none 无效验  number 数字  int 整数  float 小数  url URL地址  emil 邮箱地址 phone 手机号  idNumber 身份证号
      errorPrompt: '', // 错误提示
      defaultValue: '' // 默认值
    }
  },
  {
    type: 'textArea',
    lab: '多行输入框',
    icon: 'icon_duohang2x',
    data: {
      title: '多行输入框',
      placeholder: '请输入',
      width: 100,
      minLength: '',
      maxLength: '',
      isReadOnly: false, // 只读
      isStatistics: false, // 输入统计
      isShow: false, // 隐藏组件
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isMust: false, // 是否必填
      defaultValue: '' // 默认值
    }
  },
  {
    type: 'radio',
    lab: '单选框',
    icon: 'icon_danxuankuang2x',
    data: {
      title: '单选框',
      placeholder: '请选择',
      width: 100,
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      direction: 'column', // 排列方式row  column
      defaultValue: '', // 默认值
      dataType: 'static', // options的数据类型 static 静态类型 dynamic 动态类型
      titlab: ['选项1', '选项2', '选项3', '选项4'], // 选项 静态类型数据
      dynamicData: {
        url: '',
        method: '',
        key: '',
        value: ''
      }
    }
  },
  {
    type: 'checkbox',
    lab: '多选框',
    icon: 'icon_xialaxuanze2x',
    data: {
      title: '多选框',
      placeholder: '请选择',
      width: 100,
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      isExclude: false, // 多选互斥
      excludeOptions: [], // 多选互斥项
      direction: 'column', // 排列方式row  column
      defaultValue: [], // 默认值
      dataType: 'static', // options的数据类型 static 静态类型 dynamic 动态类型
      titlab: ['选项1', '选项2', '选项3', '选项4'], // 选项 静态类型数据
      dynamicData: {
        url: '',
        method: '',
        key: '',
        value: ''
      }
    }
  },
  {
    type: 'select',
    lab: '下拉选择框',
    icon: 'icon_xialaxuanze2x',
    data: {
      title: '下拉选择框',
      placeholder: '请选择',
      width: 100,
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 显示标题
      isMust: false, // 是否必填
      isExclude: false, // 多选互斥
      excludeOptions: [], // 多选互斥项
      defaultValue: '', // 默认值
      dataType: 'static', // options的数据类型 static 静态类型 dynamic 动态类型
      titlab: ['选项1', '选项2', '选项3', '选项4'], // 选项 静态类型数据
      dynamicData: {
        url: '',
        method: '',
        key: '',
        value: ''
      }
    }
  },
  {
    type: 'timeSelect',
    lab: '日期选择器',
    icon: 'icon_shijianxuanze2x',
    data: {
      title: '日期选择器',
      width: 100,
      placeholder: '选择日期',
      defaultWidth: 100, //  默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      minValue: '', // 最小限制
      maxValue: '', // 做大限制
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      defaultValueDay: false, // 默认日期当前天
      isMust: false, // 是否必填
      defaultValue: '', // 默认值
      dateType: 'date' // 时间类型 日（date）、 周(week)、月(month)、年(year)、日期时间(datetime)
    }
  },
  {
    type: 'number',
    lab: '数字输入框',
    icon: 'icon_shuzi2x',
    data: {
      title: '数字输入框',
      placeholder: '请输入',
      width: 100,
      minLength: '', // 最小值
      maxLength: '', // 最大值
      floatNum: '', // 小数位
      prefix: '', // 前缀
      defaultValue: '', // 默认值
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false // 是否必填
    }
  },
  {
    type: 'dateArea',
    lab: '日期区间',
    icon: 'icon_shijianqujian2x',
    data: {
      title: '日期区间',
      titlelast: '日期区间',
      startTitle: '开始时间',
      endTitle: '结束时间',
      width: 100, // 宽度
      minValue: '', // 限制开始时间节点
      maxValue: '', // 限制结束时间节点
      disableDaySt: '', // 如果是周的话 需要记录开始时间节点
      diaableDayLt: '', // 如果是周的话 需要记录开始时间节点
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      defaultValue: [], // 默认值
      dateType: 'date' // 时间类型 日（date）、 周(week)、月(month)、年(year)、日期时间(datetime)
    }
  },
  {
    type: 'rate',
    lab: '评分',
    icon: 'icon_pingfen2x',
    data: {
      title: '评分',
      width: 100, // 宽度
      defaultWidth: '1', //  默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      minValue: 0, // 最小限制
      maxValue: 5, // 做大限制
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      defaultValue: '' // 默认值
    }
  },
  {
    type: 'switch',
    lab: '开关',
    icon: 'icon_kaiguan2x',
    data: {
      title: '开关',
      width: 100, // 宽度
      defaultWidth: 1, //  默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      openText: '', // 开启文本
      closeText: '', // 关闭文本
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      defaultValue: false // 默认状态
    }
  },
  {
    type: 'slider',
    lab: '滑块选择',
    icon: 'icon_huakuai2x',
    data: {
      title: '滑块选择',
      minValue: 1, // 最小限制
      maxValue: 100, // 做大限制
      step: 1, // 每移动一步的长度
      defaultValue: 50, // 默认值
      width: 100, // 宽度
      defaultWidth: 1, // 默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: true, // 隐藏组件
      isMust: false, // 是否必填
      numShow: true // true 始终显示 false 滑动时显示
    }
  },
  {
    type: 'subform',
    lab: '子表单',
    icon: 'icon_huakuai2x',
    data: {
      title: '子表单',
      formId: '', // 关联ID
      formData: [], // 关联表单数据
      width: 100, // 宽度
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      numShow: true // true 始终显示 false 滑动时显示
    }
  },
  {
    type: 'title',
    lab: '增强控件'
  },
  {
    type: 'file',
    lab: '附件',
    icon: 'icon_fujian2x',
    data: {
      title: '附件',
      width: 100, // 宽度
      defaultWidth: 1, // 默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      fileSize: '', // 文件大小
      maxFileNum: '', // 最多上传几张
      fileTyle: '', // 文件类型 img、video、rideo、excel、word、pdf、txt、ppt
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      defaultValue: '' // 默认值
    }
  },
  {
    type: 'img',
    lab: '图片',
    icon: 'icon_tupian2x',
    data: {
      title: '图片',
      width: 100, // 宽度
      defaultWidth: 1, // 默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      fileSize: '', // 文件大小
      maxFileNum: '', // 最多上传几张
      fileType: '',
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      defaultValue: '' // 默认值
    }
  },
  {
    type: 'position',
    lab: '位置',
    icon: 'icon_weizhi2x',
    data: {
      title: '位置',
      placeholder: '请选择',
      width: 100, // 宽度
      defaultWidth: 1, // 默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      getlatAndLg: false, // 获取经纬度
      openPoints: false // 开启选点
    }
  },
  {
    type: 'location',
    lab: '省市区',
    icon: 'icon_shengshiqu2x',
    data: {
      title: '省市区',
      placeholder: '请选择',
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      isMust: false, // 是否必填
      defaultValue: '', // 默认值
      level: '1' // 1 省市 2 省市区
    }
  },
  {
    type: 'line',
    lab: '分割线',
    icon: 'icon_fengexian2x',
    data: {
      title: '分割线',
      placeholder: '请输入内容',
      width: 100, // 宽度
      height: 1, // 高度
      type: 'solid', // 线条类型
      isShowTitle: true, // 显示标题
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      isShow: false, // 隐藏组件
      color: '#D9DDE7' // 这是个色盘
    }
  },
  {
    type: 'button',
    lab: '按钮',
    icon: 'icon_anniu2x',
    data: {
      title: '按钮',
      name: '点我跳转',
      width: 100, // 宽度
      defaultWidth: 1, // 默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      isShowTitle: true, // 显示标题
      isShow: false, // 隐藏组件
      listDisplay: true,
      searchDisplay: true,
      searchType: 'EQ',
      type: 'url', // url 是跳转  phone 是拨号 只限于手机
      value: '',
      align: 'flex-start' // flex-start center flex-end  左中右
    }
  },
  {
    type: 'department',
    lab: '部门',
    icon: 'icon_bumen2x',
    data: {
      title: '部门',
      placeholder: '请选择部门',
      width: 100, // 宽度
      defaultWidth: 1, // 默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      defaultValue: '', // 默认值
      defaultValueName: '',
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      isDisplay: true, // 是否只读
      isSearch: false, // 是否开启搜索
      isShow: true, // 隐藏组件
      isMust: false // 是否必填
    }
  },
  {
    type: 'member',
    lab: '人员',
    icon: 'icon_renyuan2x',
    data: {
      title: '人员',
      placeholder: '请选择人员',
      width: 100, // 宽度
      defaultWidth: 1, // 默认宽 值为1到4 分别是 100%、50%、33.3%、25%
      defaultValue: '', // 默认值
      defaultValueName: '',
      isReadOnly: false, // 是否只读
      isShowTitle: true, // 显示标题
      isDisplay: true, // 是否只读
      isSearch: false, // 是否开启搜索
      isShow: true, // 隐藏组件
      isMust: false // 是否必填
    }
  }
]

export default ctrlDataes
