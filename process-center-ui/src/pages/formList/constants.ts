// 获取常用时间
import dayjs from 'dayjs'

export const COLUMNS = [
  {
    title: '图标',
    align: 'left',
    width: 64,
    colKey: 'icon'
  },
  {
    title: '',
    align: 'left',
    width: 380,
    minWidth: 380,
    colKey: 'name'
  },
  {
    title: '更新时间',
    ellipsis: true,
    colKey: 'lastUpdateTime',
    sorter: true,
    sortType: 'all',
    cell: (h, { row }) => {
      return h(
        'span',
        {},
        row.lastUpdateTime
          ? dayjs(row.lastUpdateTime).format('YYYY-MM-DD HH:mm:ss')
          : ''
      )
    }
  },
  {
    align: 'center',
    fixed: 'right',
    width: 90,
    minWidth: '90px',
    colKey: 'op',
    title: '操作'
  }
]
