export const RUOYI_CONFIG = {
  tags: [
    {
      tag: 'v3.8.6',
      date: '2023-06-30 08:43',
      download:
        'https://gitee.com/y_project/RuoYi-Vue/repository/archive/v3.8.6'
    },
    {
      tag: 'v3.8.5',
      date: '2023-01-01 09:09',
      download:
        'https://gitee.com/y_project/RuoYi-Vue/repository/archive/v3.8.5'
    },
    {
      tag: 'v3.8.4',
      date: '2022-09-26 08:29',
      download:
        'https://gitee.com/y_project/RuoYi-Vue/repository/archive/v3.8.4'
    },
    {
      tag: 'v3.8.3',
      date: '2022-06-27 08:23',
      download:
        'https://gitee.com/y_project/RuoYi-Vue/repository/archive/v3.8.3'
    },
    {
      tag: 'v3.8.2',
      date: '2022-04-01 08:30',
      download:
        'https://gitee.com/y_project/RuoYi-Vue/repository/archive/v3.8.2'
    },
    {
      tag: 'v3.8.1',
      date: '2021-12-31 00:00',
      download:
        'https://gitee.com/y_project/RuoYi-Vue/repository/archive/v3.8.1'
    },
    {
      tag: 'v3.8.0',
      date: '2021-12-01 08:53',
      download:
        'https://gitee.com/y_project/RuoYi-Vue/repository/archive/v3.8.0'
    }
  ],
  tagColumns: [
    {
      colKey: 'row-select',
      type: 'single',
      width: '50',
      checkProps: { allowUncheck: false }
    },
    { colKey: 'tag', title: '版本', width: '100' },
    { colKey: 'date', title: '发布时间', width: '100' },
    { colKey: 'download', title: '操作', width: '100' }
  ],
  funs: [
    {
      id: 'oa',
      name: 'OA功能模块',
      desc: '能够发起申请、查看申请、办理流程',
      romd: '★★★★★',
      type: 'module'
    },
    {
      id: 'bis',
      name: '业务流程模块',
      desc: '支持让业务人员进行流程设计、流程管理',
      romd: '★★★★★',
      type: 'module'
    },
    {
      id: 'dev',
      name: '开发流程模块',
      desc: '支持让业务人员进行流程设计、流程管理',
      romd: '★★★★★',
      type: 'module'
    },
    {
      id: 'instance',
      name: '实例管理模块',
      desc: '支持让业务人员进行流程实例管理',
      romd: '★★★★★',
      type: 'module'
    },
    {
      id: 'form',
      name: '表单管理模块',
      desc: '支持让业务人员进行表单设计、表单管理',
      romd: '★★★★★',
      type: 'module'
    }
  ],
  funColumns: [
    {
      colKey: 'row-select',
      type: 'multiple',
      width: '50',
      disabled: ({ row }) => row.type === 'app'
    },
    { colKey: 'name', title: '模块', width: '100' },
    { colKey: 'desc', title: '描述', width: '100' },
    { colKey: 'romd', title: '推荐指数', width: '100' }
  ]
}
