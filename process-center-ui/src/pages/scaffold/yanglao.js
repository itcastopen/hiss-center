export const YANGLAO_CONFIG = {
  funs: [
    {
      id: 'oa',
      name: 'OA功能模块',
      desc: '能够发起申请、查看申请、办理流程',
      romd: '★★★★★',
      type: 'module'
    }
  ],
  funColumns: [
    {
      colKey: 'row-select',
      type: 'multiple',
      width: '50',
      disabled: ({ row }) => row.type === 'app'
    },
    { colKey: 'name', title: '模块', width: '100' },
    { colKey: 'desc', title: '描述', width: '100' },
    { colKey: 'romd', title: '推荐指数', width: '100' }
  ]
}
