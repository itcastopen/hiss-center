// 获取常用时间
import dayjs from "dayjs";

export const COLUMNS = [
  {
    title: '序号',
    align: 'left',
    width: 80,
    minWidth: 80,
    colKey: 'serial-number'
  },
  {
    title: '用户名',
    align: 'left',
    width: 160,
    minWidth: 100,
    colKey: 'name'
  },
  {
    title: '账号名',
    colKey: 'username',
    width: 300,
    sorter: true,
    sortType: 'all'
  },
  {
    title: '关联应用',
    minWidth: 150,
    ellipsis: true,
    colKey: 'appName'
  },
  {
    title: '外部管理员ID',
    minWidth: 150,
    ellipsis: true,
    colKey: 'externalAdminId'
  },
  {
    title: '状态',
    ellipsis: true, // 文字超出宽度时显示省略号
    colKey: 'status',
    // 添加筛选
    filter: {
      type: 'single',
      list: [
        {
          label: '停用',
          value: 0
        },
        {
          label: '正常',
          value: 1
        }
      ],
      showConfirmAndReset: true
    },
    cell: (h, { row }) => {
      const statusList = {
        0: {
          label: '停用'
        },
        1: {
          label: '正常'
        }
      }
      return h(
        'span',
        {
          class: `status-dot status-dot-${row.status}`
        },
        statusList[row.status].label
      )
    }
  },
  {
    title: '创建时间',
    ellipsis: true,
    colKey: 'cTime',
    sorter: true,
    sortType: 'all',
    cell: (h, { row }) => {
      return h(
        'span',
        {},
        row.createdTime
          ? dayjs(row.createdTime).format('YYYY-MM-DD HH:mm:ss')
          : ''
      )
    }
  },
  {
    align: 'center',
    fixed: 'right',
    width: 300,
    minWidth: '280px',
    colKey: 'op',
    title: '操作'
  }
]
