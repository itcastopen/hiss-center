import Layout from '@/layouts/index.vue'
import ProcessIcon from '@/assets/icon_lcgl_nor.svg'
import ProcessInstanceIcon from '@/assets/icon_lcsl_nor.svg'
import FormIcon from '@/assets/test-img/icon_bdgl_nor.svg'
import AppIcon from '@/assets/test-img/icon_yygl_nor.svg'
import UserIcon from '@/assets/icon_yhgl_nor.svg'
import Ruoyi from '@/assets/icon_ry_nor.svg'
import Yanglao from '@/assets/icon_yl_nor.svg'

const normalRouter = [
  {
    path: '/application',
    name: 'application',
    component: Layout,
    redirect: '/application/index',
    meta: { title: '应用管理', icon: AppIcon, single: true },
    children: [
      {
        path: 'index',
        name: 'applicationIndex',
        component: () => import('@/pages/application/index.vue'),
        meta: {
          title: '应用列表',
          singles: true
        }
      }
    ]
  },
  {
    path: '/systemuser',
    name: 'systemuser',
    component: Layout,
    redirect: '/systemuser/index',
    meta: { title: '用户管理', icon: UserIcon, single: true },
    children: [
      {
        path: 'index',
        name: 'userIndex',
        component: () => import('@/pages/systemuser/index.vue'),
        meta: {
          title: '用户列表',
          singles: true
        }
      }
    ]
  },
  {
    path: '/flowBis',
    name: 'flowBisList',
    component: Layout,
    redirect: '/flowBis/index',
    meta: { title: '流程管理', icon: ProcessIcon, single: true },
    children: [
      {
        path: 'index',
        name: 'flowBisIndex',
        component: () => import('@/pages/process/bis/index.vue'),
        meta: {
          title: '流程管理',
          singles: true
        }
      },
      {
        path: 'newBis',
        name: 'hissBisProcessAdd',
        component: () => import('@/pages/process/bis/newBis.vue'),
        meta: {
          title: '流程设计',
          singles: true
        }
      },
      {
        path: 'editBis',
        name: 'hissBisProcessEdit',
        component: () => import('@/pages/process/bis/editBis.vue'),
        meta: {
          title: '流程设计',
          singles: true
        }
      },
      {
        path: 'editDev',
        name: 'hissDevProcessEdit',
        component: () => import('@/pages/process/bis/editDev.vue'),
        meta: {
          title: '流程设计',
          singles: true
        }
      },
      {
        path: 'newDev',
        name: 'hissDevProcessAdd',
        component: () => import('@/pages/process/bis/newDev.vue'),
        meta: {
          title: '流程设计',
          singles: true
        }
      }
    ]
  },
  {
    path: '/flowInstance',
    name: 'flowInstanceList',
    component: Layout,
    parent: 'flowBisList',
    redirect: '/flowInstance/index',
    meta: { title: '流程实例', icon: ProcessInstanceIcon, single: true },
    children: [
      {
        path: 'index',
        name: 'flowInstanceIndex',
        component: () => import('@/pages/process/instance/index.vue'),
        meta: {
          title: '流程管理',
          singles: true
        }
      },
      {
        path: 'handle',
        name: 'hissProcessHandle',
        component: () => import('@/pages/process/instance/handle.vue'),
        meta: {
          title: '流程设计',
          singles: true
        }
      }
    ]
  },
  {
    path: '/formList',
    name: 'formList',
    component: Layout,
    redirect: '/formList/list',
    meta: { title: '表单管理', icon: FormIcon, single: true },
    children: [
      {
        path: 'list',
        name: 'ListBase',
        component: () => import('@/pages/formList/index.vue'),
        meta: {
          title: '表单设计',
          singles: true
        }
      },
      {
        path: 'data',
        name: 'formData',
        component: () => import('@/pages/formList/data.vue'),
        meta: {
          title: '表单数据',
          singles: true
        }
      },
      {
        path: 'design',
        name: 'fromdes',
        component: () => import('@/pages/form/index.vue'),
        meta: {
          title: '表单设计',
          singles: true
        }
      }
    ]
  },
  {
    path: '/scaffold-js',
    name: 'scaffold-js',
    component: Layout,
    redirect: '/scaffold-js/ruoyi',
    meta: { title: '若依集成', icon: Ruoyi, single: true },
    children: [
      {
        path: 'ruoyi',
        name: 'scaffoldRuoyi',
        component: () => import('@/pages/scaffold/ruoyi.vue'),
        meta: {
          title: '若依集成',
          singles: true
        }
      }
    ]
  },
  {
    path: '/scaffold-ts',
    name: 'scaffold-ts',
    component: Layout,
    redirect: '/scaffold-ts/yanglao',
    meta: { title: '养老集成', icon: Yanglao, single: true },
    children: [
      {
        path: 'yanglao',
        name: 'scaffoldYanglao',
        component: () => import('@/pages/scaffold/yanglao.vue'),
        meta: {
          title: '养老集成',
          singles: true
        }
      }
    ]
  }
]

export default normalRouter
