import { createPinia } from 'pinia'
import { createPersistedState } from 'pinia-plugin-persistedstate'

const store = createPinia()
store.use(createPersistedState())

// 做数据持久化
// store.use(piniaPluginPersistedstate)

export { store }

export * from './modules/notification'
export * from './modules/permission'
export * from './modules/user'
export * from './modules/setting'
export * from './modules/tabs-router'
export * from './modules/formData'

export default store
