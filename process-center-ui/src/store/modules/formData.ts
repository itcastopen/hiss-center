import { defineStore } from 'pinia'

export const useFormData = defineStore('formData', {
  state: () => ({
    baseFormData: {},
    formData: {},
    submitFormData: {}
  }),
  getters: {
    getBaseFormData: (state) => {
      return state.baseFormData
    },
    getFormData: (state) => {
      return state.formData
    },
    getSubmitFormData: (state) => {
      return state.submitFormData
    }
  },
  actions: {
    async setBaseFormData(data: Object) {
      this.baseFormData = data
    },
    async setFormData(data: Object) {
      this.formData = data
    },
    async setSubmitFormData(data: Object) {
      this.submitFormData = data
    }
  },
  persist: true
})
