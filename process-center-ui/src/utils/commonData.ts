// tab数据
export const tabData = [
  {
    id: 1,
    name: '线索'
  },
  {
    id: 2,
    name: '线索池子'
  }
]

export const hissProcessStatus = [
  {
    id: null,
    value: '全部'
  },
  {
    id: 'PREPARE',
    value: '待发起'
  },
  {
    id: 'ACTIVE',
    value: '办理中'
  },
  {
    id: 'COMPLETE',
    value: '已完成'
  },
  {
    id: 'CANCEL',
    value: '已取消'
  }
]
