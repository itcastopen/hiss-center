const API_URL = {
    "commonMessageURL":"/v1/receiver",
    "searchFormByName":"/hiss/v1/process/searchFormByName",
    "getFormFieldsUrl":"/hiss/v1/process/getFormFields"
}
export default API_URL
