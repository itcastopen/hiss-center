import request from '@/utils/request'
import url from './ApiUrl'
import mid from './MessageId'
import {packageMessage} from './PackageMessage'

/**
 * 开始一个业务流程
 * @param data
 * @returns {*}
 */
export function startBisDesign(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processBisDesginStart,data,messageAuth)
    })
}

/**
 * 转换成开发模式
 * @param data
 * @returns {*}
 */
export function startToDevDesign(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processBisDesginToDev,data,messageAuth)
    })
}

/**
 * 设计部署
 * @param data
 * @returns {*}
 */
export function modelDeploment(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.modelDeploment,data)
    })
}

/**
 * 启动一个预备流程
 * @param data
 * @returns {*}
 */
export function startPreProcessDesign(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.preProcessBisDesginStart,data,messageAuth)
    })
}

/**
 * 保存设计搞
 * @param data
 * @return {*}
 */
export function saveBisDesign(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processBisDesginSave,data)
    })
}

/**
 * 获取设计稿
 * @param data
 * @return {*}
 */
export function getBisDesign(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processBisDesginGet,data)
    })
}

/**
 * 远程搜索自定义变量的数据
 * @param data
 * @return {*}
 */
export function searchSelectVariableQuery(data) {
    return new Promise((resolve,reject)=>{
        request({
            url: url.commonMessageURL,
            method: 'post',
            data :packageMessage(mid.queryClientVariableData,data)
        }).then(res=>{
            resolve(res)
        }).catch(res=>{
            console.log("================ 使用测试数据 ================")
            let data = {
                data:{
                    result:[
                        {"id":"1","name":"部门1","select":false},
                        {"id":"2","name":"王五","select":true},
                        {"id":"3","name":"部门2","select":false},
                        {"id":"4","name":"小刘","select":true},
                        {"id":"5","name":"赵二","select":true}
                    ]
                }
            }
            resolve(data)
        })
    })
}

/**
 * 远程加载自定义变量的树形数据
 * @param data
 * @return {*}
 */
export function searchSelectVariableTree(data) {
    return new Promise((resolve,reject)=>{
        request({
            url: url.commonMessageURL,
            method: 'post',
            data :packageMessage(mid.queryClientVariableData,data)
        }).then(res=>{
            resolve(res)
        }).catch(res=>{
            console.log("================ 使用测试数据 ================")
            let data = {
                data:{
                    result:[
                        {"id":"1","name":"部门1","select":false},
                        {"id":"2","name":"王五","select":true},
                        {"id":"3","name":"部门2","select":false},
                        {"id":"4","name":"小刘","select":true},
                        {"id":"5","name":"赵二","select":true}
                    ]
                }
            }
            resolve(data)
        })
    })
}

/**
 * 远程搜索用户
 * @param data
 * @return {*}
 */
export function searchHissForm(data) {
    return new Promise((resolve,reject)=>{
        request({
            url: url.commonMessageURL,
            method: 'post',
            data :packageMessage(mid.queryFormInfoData,data)
        }).then(res=>{
            console.log(res)
            resolve(res)
        }).catch(res=>{
            console.log("================ 使用测试数据 ================")
            let data = {
                data:{
                    flag:[
                        {
                            id:'1',
                            name:'请假单',
                            fields:[
                                {fieldName:'username',fieldLabel:'请假人'},
                                {fieldName:'time',fieldLabel:'请假时间'},
                                {fieldName:'reson',fieldLabel:'请假理由'}
                            ]
                        },
                        {
                            id:'2',
                            name:'调休单',
                            fields:[
                                {fieldName:'username',fieldLabel:'调休人'},
                                {fieldName:'time',fieldLabel:'调休时间'},
                                {fieldName:'reson',fieldLabel:'调休理由'}
                            ]
                        }
                    ]
                }
            }
            resolve(data)
        })
    })
}

/**
 * 获取租户自定的变量
 * @param data
 * @return {*}
 */
export function queryTenantSingleUserVariable(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.queryTenantSingleUserVariable, data)
    })
}

/**
 * 远程搜索用户
 * @param data
 * @return {*}
 */
export function searchExternalForm(keyword) {
    let data = {
        type:'1',
        query:keyword
    }
    return new Promise((resolve,reject)=>{
        request({
            url: url.commonMessageURL,
            method: 'post',
            data :packageMessage(mid.queryFormInfoData,data)
        }).then(res=>{
            console.log(res)
            resolve(res)
        }).catch(res=>{
            console.log("================ 使用测试数据 ================")
            let data = {
                data:{
                    flag:[
                        {
                            id:'1',
                            name:'请假单',
                            fields:[
                                {fieldName:'username',fieldLabel:'请假人'},
                                {fieldName:'time',fieldLabel:'请假时间'},
                                {fieldName:'reson',fieldLabel:'请假理由'}
                            ]
                        },
                        {
                            id:'2',
                            name:'调休单',
                            fields:[
                                {fieldName:'username',fieldLabel:'调休人'},
                                {fieldName:'time',fieldLabel:'调休时间'},
                                {fieldName:'reson',fieldLabel:'调休理由'}
                            ]
                        }
                    ]
                }
            }
            resolve(data)
        })
    })
}

/**
 * 获取发起人的字段列表
 * @param id    内外部表单ID
 * @param type  内部还是外部表单
 * @returns {[]|[{id:'字段ID',name:'字段名称']}}
 */
export function getFormFields(id,type) {
    if(id && type) {
        return [
            {
                "id":'username',
                "name":'用户名'
            },{
                "id":'password',
                "name":'密码'
            }
        ]
        return request({
            url: url.getFormFieldsUrl,
            method: 'post',
            data: {
                id, type
            }
        })
    }
}

/**
 * 通过名称搜索表单
 * @param id    内外部表单ID
 * @param type  内部还是外部表单
 * @returns {[]|[{id:'表单ID',name:'表单名称']}}
 */
export function searchFormByName(id,type) {
    if(!id || !type){
        return []
    }
    return request({
        url: url.getFormFieldsUrl,
        method: 'post',
        data:{
            id,type
        }
    })
}



