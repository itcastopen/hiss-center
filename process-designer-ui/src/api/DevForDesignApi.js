import request from '@/utils/request'
import url from './ApiUrl'
import mid from './MessageId'
import {packageMessage} from './PackageMessage'

/**
 * 保存设计的文件
 * @param data
 * @returns {*}
 */
export function saveDevDesign(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processDevDesginSave,data)
    })
}

/**
 * 获取设计的文件
 * @param data
 * @returns {*}
 */
export function getDevDesign(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processDevDesginGet,data)
    })
}

/**
 * 开始一个流程
 * @param data
 * @returns {*}
 */
export function startDevDesign(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processDevDesginStart,data,messageAuth)
    })
}

/**
 * 催办一个流程
 * @param data
 * @returns {*}
 */
export function processActionExpedite(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionExpedite,data,messageAuth)
    })
}

/**
 * 重新发起一个流程
 * @param data
 * @returns {*}
 */
export function processActionRestart(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionRestart,data,messageAuth)
    })
}


/**
 * 提交一个流程
 * @param data
 * @returns {*}
 */
export function processActionSubmit(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionSubmit,data,messageAuth)
    })
}


/**
 * 发起一个流程
 * @param data
 * @returns {*}
 */
export function processActionSend(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionSend,data,messageAuth)
    })
}

/**
 * 暂存一个流程
 * @param data
 * @returns {*}
 */
export function processActionDraft(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionDraft,data,messageAuth)
    })
}

/**
 * 同意一个流程
 * @param data
 * @returns {*}
 */
export function processActionApprove(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionApprove,data,messageAuth)
    })
}

/**
 * 不同意一个流程
 * @param data
 * @returns {*}
 */
export function processActionReject(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionReject,data,messageAuth)
    })
}

/**
 * 知会一个流程
 * @param data
 * @returns {*}
 */
export function processActionNotification(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionNotification,data,messageAuth)
    })
}

/**
 * 抄送一个流程
 * @param data
 * @returns {*}
 */
export function processActionCc(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionCc,data,messageAuth)
    })
}

/**
 * 认领一个流程
 * @param data
 * @returns {*}
 */
export function processActionClaim(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionClaim,data,messageAuth)
    })
}

/**
 * 归还一个流程
 * @param data
 * @returns {*}
 */
export function processActionUnclaim(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionUnclaim,data,messageAuth)
    })
}

/**
 * 委派一个流程
 * @param data
 * @returns {*}
 */
export function processActionDelegate(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionDelegate,data,messageAuth)
    })
}

/**
 * 回退一个流程
 * @param data
 * @returns {*}
 */
export function processActionRollback(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionRollback,data,messageAuth)
    })
}

/**
 * 取消一个流程
 * @param data
 * @returns {*}
 */
export function processActionCancel(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionCancel,data,messageAuth)
    })
}

/**
 * 跳转一个流程
 * @param data
 * @returns {*}
 */
export function processActionJump(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionJump,data,messageAuth)
    })
}

/**
 * 知悉一个流程
 * @param data
 * @returns {*}
 */
export function processActionRead(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionRead,data,messageAuth)
    })
}

/**
 * 触发流程信号等信息
 * @param data
 * @returns {*}
 */
export function processActionTrigger(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionTrigger,data,messageAuth)
    })
}

/**
 * 撤回一个流程
 * @param data
 * @returns {*}
 */
export function processActionWithdraw(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionWithdraw,data,messageAuth)
    })
}

/**
 * 设计部署
 * @param data
 * @returns {*}
 */
export function modelDeploment(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.modelDeploment,data)
    })
}

/**
 * 后加签一个流程
 * @param data
 * @returns {*}
 */
export function processActionAfterSign(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionAfterSign,data,messageAuth)
    })
}

/**
 * 并加签一个流程
 * @param data
 * @returns {*}
 */
export function handlerActionParallelSign(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionParallelSign,data,messageAuth)
    })
}


/**
 * 前加签一个流程
 * @param data
 * @returns {*}
 */
export function processActionBeforeSign(data,messageAuth) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processActionBeforeSign,data,messageAuth)
    })
}

/**
 * 获取开发者的预览文件
 * @param data
 * @returns {*}
 */
export function getDevViewer(data) {
    return request({
        url: url.commonMessageURL,
        method: 'post',
        data :packageMessage(mid.processDevViewerGet,data)
    })
}
