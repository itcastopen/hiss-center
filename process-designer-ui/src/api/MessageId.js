import {modelDeploment, queryTenantSingleUserType} from "@/api/BusinessForDesignApi";
import {formActionDelete, formActionFields} from "@/api/FormApi";
import {processActionExpedite} from "@/api/DevForDesignApi";

const MESSAGE_ID = {
    "modelDeploment":"MODEL_TO_DEPLOYMENT",// 设计部署
    "processBisDesginSave":"FLOW_M_SAVE_MODEL_FOR_BIS",
    "processBisDesginGet":"FLOW_M_GET_MODEL_FOR_BIS",
    "processBisDesginStart":"FLOW_M_START_MODEL_FOR_BIS",// 流程
    "preProcessBisDesginStart":"FLOW_M_PRE_START_MODEL_FOR_BIS",// 启动预备流程
    "processBisDesginToDev":"FLOW_M_TODEV_MODEL_FOR_BIS",
    "processDevDesginSave":"FLOW_M_SAVE_MODEL_FOR_DEV",
    "processDevDesginGet":"FLOW_M_GET_MODEL_FOR_DEV",
    "processDevDesginStart":"FLOW_M_START_MODEL_FOR_DEV",
    "processDevViewerGet":"GET_INSTANCE_DEV_VIEWER",
    // ===================== 流程办理的内容 =======================
    "processActionDraft":"DRAFT_TASK",//暂存
    "processActionRestart":"RESTART_TASK",//重新发起
    "processActionExpedite":"EXPEDITE_TASK",//催办
    "processActionSubmit":"SUBMIT_TASK",//提交
    "processActionSend":"SEND_TASK",//发起
    "processActionApprove":"APPROVE_TASK",//同意
    "processActionReject":"REJECT_TASK",//不同意
    "processActionNotification":"NOTIFICATION_TASK",//知会
    "processActionCc":"CC_TASK",//抄送
    "processActionClaim":"CLAIM_TASK",//认领
    "processActionUnclaim":"UNCLAIM_TASK",//归还
    "processActionDelegate":"DELEGATE_TASK",//委派
    "processActionRollback":"ROLLBACK_TASK",//回退
    "processActionCancel":"CANCEl_TASK",//取消
    "processActionJump":"JUMP_TASK",//跳转
    "processActionWithdraw":"WITHDRAW_TASK",//撤回
    "processActionTrigger":"RECEIVED_TASK",//触发消息或信号
    "processActionBeforeSign":"BEFORE_SIGN_TASK",//前加签
    "processActionAfterSign":"AFTER_SIGN_TASK",//后加签
    "processActionParallelSign":"PARALLEL_SIGN_TASK",// 并加签
    "processActionRead":"READ_TASK",//知晓
    // ============================ 业务模式 ==============================
    // ===================== 需要和客户端交互的接口 =======================
    "queryTenantSingleUserVariable":"SYS_GET_VARIABLE",//查询租户自定义的变量
    "queryClientVariableData":"SERVER_VARIABLES_METHOD",//查询自定义变量类型对应的数据
    "queryFormInfoData":"ACTIVITI_DEFINITION_GET_FORM_DIFINITION",//查询表单信息
    // ===================== 表单 =======================
    "formActionFields":"GET_MY_FORM_FIELD",// 获取表单字段
    "formActionDelete":"GET_MY_FORM_DATA_DELETE",// 删除表单数据
    "formActionDataList":"GET_MY_FORM_DATA_LIST",// 获取表单列表数据
}
export default MESSAGE_ID
