import { createApp } from 'vue'
// 以下为bpmn工作流绘图工具的样式
import 'bpmn-js/dist/assets/diagram-js.css' // 左边工具栏以及编辑节点的样式
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css'
import 'bpmn-js-properties-panel/dist/assets/properties-panel.css'
// element元素
import 'element-plus/dist/index.css'
import ElementPlus from 'element-plus'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// highlightjs高亮
import 'highlight.js/styles/stackoverflow-light.css'
import hljs from 'highlight.js/lib/core';
import xml from 'highlight.js/lib/languages/xml';
import hljsVuePlugin from "@highlightjs/vue-plugin";
// 引入axios
import request from './utils/request';

import App from './App.vue'
// 路由
import routes from './router'
import {createRouter, createWebHashHistory,createWebHistory} from "vue-router/dist/vue-router";

hljs.registerLanguage('xml', xml);

const config = {
    routerName:'',
    routerPath:'',
    routerMeta:{},
    serverUrl: 'http://localhost:8081/',
    history: 'Hash'
}

function createDesign(config){
    window['HISS_URL'] = config.serverUrl
    const app = createApp(App)
    for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
        app.component(key, component)
    }
    // 重新路由path，以便可以网页刷新
    for (let i = 0; i < routes.length; i++) {
        let router = routes[i]
        if(config.routerName==router.name){
            router.path=config.routerPath
            router.meta = config.routerMeta
            router.props = config.routerMeta
            router.query = config.routerMeta
            router.params = config.routerMeta
        }
    }
    // console.log('========加载路由========:',routes)
    // 创建路由
    let history = '';
    if(!config.history || config.history=='Hash'){
        history = createWebHashHistory()
    }else{
        history = createWebHistory()
    }
    const router = createRouter({
        history,
        routes
    })
    app.hissConfig = config
    app.request = request
    app.use(ElementPlus)
    app.use(hljsVuePlugin)
    app.use(router)
    return app
}

window.createDesign = createDesign

export default createDesign
