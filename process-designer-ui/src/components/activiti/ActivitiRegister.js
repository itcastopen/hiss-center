import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const register = new ActivitiRegister();
window['register'] = register;
import activitiMapping from './ActivitiMapping'
// 导入字段组件
require('./parts/StartEventInitiator')
require('./parts/EventType')
require('./parts/ListenerType')
require('./parts/JavaClass')
require('./parts/DelegateExpression')
require('./parts/ListenerExpression')
require('./parts/ListenerResultVariable')
require('./parts/Format')
require('./parts/Resource')
require('./parts/Script')
require('./parts/ScriptType')
require('./parts/Interrupting')
require('./parts/PropertyName')
require('./parts/PropertyValue')
require('./parts/PropertyType')
require('./parts/ListenerId')
require('./parts/Fields')
// =========== throwEvent ===================
require('./parts/ThrowEventType')
require('./parts/SignalName')
require('./parts/MessageName')
require('./parts/ErrorCode')
require('./parts/OnTransaction')
require('./parts/ListenerCustomType')
require('./parts/CustomJavaClass')
require('./parts/CustomExpression')
require('./parts/CustomDelegateExpression')
// =========== 多实例 ===================
require('./parts/CollectionVariableName')
require('./parts/ElementVariable')
require('./parts/LoopCardinality')
require('./parts/Sequential')
require('./parts/CompletionCondition')
// =========== UserTask ===================
require('./parts/Assignee')
require('./parts/DueDate')
require('./parts/CandidateUsers')
require('./parts/CandidateGroups')
require('./parts/Priority')
require('./parts/LoopCardinality')
require('./parts/SkipExpression')
// =========== ServiceTask ===================
require('./parts/ServiceTaskType')
require('./parts/ResultVariableName')
// =========== ScriptTask ===================
require('./parts/ResultVariable')
// =========== SendTask ===================
require('./parts/MailTo')
require('./parts/MailHtml')
require('./parts/MailSubject')
require('./parts/MailAttachments')
// =========== SequenceFlow ===================
require('./parts/ConditionExpression')
// =========== CallActivity ===================
require('./parts/CalledElement')
// =========== messageEventDefinition  ===================
require('./parts/CorrelationKey')
require('./parts/MessageExpression')

const deleteIds = ["multiInstance"]
function ActivitiRegister(){
    this.components= {};
    this.groups = {};
    this.regComponent = function (id, component) {
        // console.log("===============================regComponent=:"+id)
        this.components[id] = component;
    };
    this.regGroup = function (module) {
        let group = module.default
        // console.log("===============================regGroup=:"+group.id)
        this.groups[group.id] = group;
    };
   this.findGroup = function (groups,element, translate,injector){
       if(groups.length>0){
           const general = groups[0];
           general.shouldOpen=true;
           general.open=true
       }
       var type = element.type;
       // console.log("===============================groupMapping:"+groupMapping)
       let doc = groups[1];
       if(groups.length>1){
           if(doc.id=="documentation"){
               groups.splice(1,1);
           }
       }
       // 删除多于的文档
       for (let j = 0; j < deleteIds.length; j++) {
           for (let i = 0; i < groups.length; i++) {
               if(groups[i].id==deleteIds[j]){
                   groups.splice(i,1);
               }
           }
       }
       var groupMapping = activitiMapping[type];
       let businessObject = getBusinessObject(element);
       if(businessObject.eventDefinitions){
           if(businessObject.eventDefinitions[0]){
               let eventDefinition = businessObject.eventDefinitions[0]['$type'];
               groupMapping = activitiMapping[eventDefinition];
           }
       }
       if(groupMapping) {
            for(let key in groupMapping){
                let groupFields = groupMapping[key];
                let entries = [];
                for (let i = 0; i < groupFields.length; i++) {
                    let field = this.findEntries(groupFields[i],translate);
                    // console.log(groupFields[i]+"===============================field:"+field)
                    if(field) {
                        entries.push(field)
                    }
                }
                let groupComponent = this.groups[key];
                groupComponent.label = translate(groupComponent.label)
                groupComponent.entries = entries;
                if(groupComponent.function) {
                    let atts = groupComponent.function.call(this,groups,element, translate,injector,this);
                    groupComponent = {...groupComponent,...atts}
                }
                groups.push(groupComponent)
            }
       }
       // console.log("===============================group:"+groups)
       if(doc){
           groups.push(doc)
       }
       // console.log(groups)
       return groups;
   };
    this.findEntries = function (id, translate){
        // console.log(this.components)
        // console.log(id+"================this.components===============:"+this.components)
        return this.components[id];
    }
}

export default register

// 导入注册器
const adviceGroup = require('./group/AdviceGroup')
register.regGroup(adviceGroup)
const executionListenerGroup = require('./group/ExecutionListenerGroup.js')
register.regGroup(executionListenerGroup)
const extensionPropertiesGroup = require('./group/ExtensionPropertiesGroup.js')
register.regGroup(extensionPropertiesGroup)
const taskListenerGroup = require('./group/TaskListenerGroup.js')
register.regGroup(taskListenerGroup)
const multiInstanceGroup = require('./group/MultiInstanceGroup.js')
register.regGroup(multiInstanceGroup)
const serviceTaskGroup = require('./group/ServiceTaskGroup.js')
register.regGroup(serviceTaskGroup)
const activitiListenerGroup = require('./group/ActivitiListenerGroup.js')
register.regGroup(activitiListenerGroup)



