import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";
import {addMultiInstanceFactory, getLoopCharacteristics, removeMultiInstanceFactory} from "@/components/activiti/util";
import {ListGroup} from "@bpmn-io/properties-panel";

const multiInstance = {
    id: 'multiInstance',
    label: 'MultiInstance',
    component:ListGroup,
    function:function (groups,element, translate,injector,register){
        let a = MultiInstanceProps({
            element,
            translate,
            injector,
            register
        })
        return a;
    }
};
export default multiInstance;

function MultiInstanceProps(props) {
    const {
        element,injector,register
    } = props;

    const bpmnFactory = injector.get('bpmnFactory'),
        commandStack = injector.get('commandStack');
    const translate = injector.get('translate');
    const businessObject = getBusinessObject(element);
    const loopCharacteristics = getLoopCharacteristics(businessObject);
    const items = []
    if(loopCharacteristics) {
        let id = element.id + '-loop-char-0';
        items.push({
            id,
            label: '  ',
            entries: getEntries(element, id, register, translate, loopCharacteristics),
            remove: removeMultiInstanceFactory({commandStack, element, loopCharacteristics})
        })
        return {
            items
        };
    }else{
        return {
            items,
            add: addMultiInstanceFactory({element, bpmnFactory, commandStack})
        };
    }
}

function getEntries(element,id,register,translate,loopCharacteristics){
    let arrs = ['sequential','collectionVariableName','elementVariable','completionCondition','loopCardinality'];
    const  list = []
    for (let i = 0; i < arrs.length; i++) {
        let entries = register.findEntries(arrs[i],translate)
        entries = {...entries}
        entries.id = id;
        entries.loopCharacteristics =loopCharacteristics;
        list.push(entries)
    }
    return list
}
