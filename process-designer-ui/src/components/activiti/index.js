import ActivitiPropertiesProvider from './ActivitiPropertiesProvider.js';

export default {
  __init__: [ 'activitiPropertiesProvider' ],
  activitiPropertiesProvider: [ 'type', ActivitiPropertiesProvider ]
};
