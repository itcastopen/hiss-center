import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'candidateUsers',
  component: CandidateUsers,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function CandidateUsers(props) {
  const {
    element
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const businessObject = getBusinessObject(element);
  const getValue = () => {
    return businessObject.get('activiti:candidateUsers');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'activiti:candidateUsers': value
      }
    });
  };
  return TextFieldEntry({
    element,
    id: element.id+'-candidateUsers',
    label: translate('Candidate users'),
    getValue,
    setValue,
    debounce
  });
}
