import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {updateMultiInstance} from "@/components/activiti/util";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'collectionVariableName',
  component: CollectionVariableName,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function CollectionVariableName(props) {
  const { element, id } = props;
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const bpmnFactory = useService('bpmnFactory');
  const commandStack = useService('commandStack');
  const businessObject = getBusinessObject(element);
  let loopCharacteristics = businessObject.get('loopCharacteristics');
  const getValue = () => {
    if(loopCharacteristics&&loopCharacteristics.$attrs){
      return loopCharacteristics.$attrs['activiti:collection'] || '';
    }
    return '';
  }

  const setValue = value => {
    return updateMultiInstance(element,businessObject,bpmnFactory,commandStack,{
      'activiti:collection': value
    })
  }

  return html`<${TextFieldEntry}
    id=${ id }
    element=${ element }
    description=${ translate('If defined with {}, it is the specified set, and vice versa, it is a variable') }
    label=${ translate('CollectionVariableName') }
    getValue=${ getValue }
    setValue=${ setValue }
    debounce=${ debounce }
  />`
}

