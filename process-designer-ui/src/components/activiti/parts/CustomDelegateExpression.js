import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
    id: 'customDelegateExpression',
    component: CustomDelegateExpression,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function CustomDelegateExpression(props) {
    const {
        element,
        businessObject,
        id
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const getValue = () => {
        return businessObject.get('activiti:customPropertiesResolverDelegateExpression');
    };
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: businessObject,
            properties: {
                'activiti:customPropertiesResolverDelegateExpression': value || ''
            }
        });
    };
    return TextFieldEntry({
        element,
        id:id+"-delegateExpression",
        label: translate('CustomDelegateExpression'),
        getValue,
        setValue,
        debounce
    });
}
