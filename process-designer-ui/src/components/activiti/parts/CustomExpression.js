import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
  id: 'customExpression',
  component: CustomExpression,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function CustomExpression(props) {
  const {
    element,
    businessObject,
    id = ''
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const getValue = () => {
    return businessObject.get('activiti:customPropertiesResolverExpression');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'activiti:customPropertiesResolverExpression': value || ''
      }
    });
  };
  return TextFieldEntry({
    element,
    id:id+"-customExpression",
    label: translate('CustomExpression'),
    getValue,
    setValue,
    debounce
  });
}
