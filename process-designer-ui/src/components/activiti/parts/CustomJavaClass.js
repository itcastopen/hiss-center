import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
  id: 'customJavaClass',
  component: CustomJavaClass,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function CustomJavaClass(props) {
  const {
    element,
    businessObject,
    id = ''
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const getValue = () => {
    return businessObject.get('activiti:customPropertiesResolverClass');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'activiti:customPropertiesResolverClass': value || ''
      }
    });
  };
  return TextFieldEntry({
    element,
    id:id+"-customJavaClass",
    label: translate('CustomJavaClass'),
    getValue,
    setValue,
    debounce
  });
}
