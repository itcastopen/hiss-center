import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
    id: 'delegateExpression',
    component: DelegateExpression,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function DelegateExpression(props) {
    const {
        element,
        businessObject,
        id
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const getValue = () => {
        return businessObject.get('activiti:delegateExpression');
    };
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: businessObject,
            properties: {
                'activiti:delegateExpression': value || ''
            }
        });
    };
    const validate = value => {
        if(!value){
            return translate('value required')
        }
    }
    return TextFieldEntry({
        element,
        id,
        label: translate('Delegate expression'),
        getValue,
        setValue,
        debounce,
        validate
    });
}
