import {CollapsibleEntry, isTextFieldEntryEdited, ListEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";
import {addExtensionElements, createElement, getExtensionElementsList, removeExtensionElements} from "../util"
import {without} from 'min-dash';
import {is} from "bpmn-js/lib/util/ModelUtil";

const component = {
    id: 'fields',
    component: Fields,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function Fields(props) {
    const { element, id,listener,register,businessObject } = props;
    const bpmnFactory = useService('bpmnFactory');
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const {fields,add,remove} = parseListener(listener,bpmnFactory,businessObject,commandStack,element)
    return jsx(ListEntry, {
        id: id,
        register:register,
        translate:translate,
        element: element,
        label: translate('Field injection'),
        items: fields,
        component: Field,
        onAdd: add,
        onRemove: remove,
        compareFn: compareName,
        autoFocusEntry: true
    });
}
// 如果是ExtensionElements添加字段
// 如果是activiti:ExecutionListener", "activiti:TaskListener"
function parseListener(listener,bpmnFactory,businessObject,commandStack,element){
    if(is(listener,'bpmn:ExtensionElements')){
        return {
            fields: getExtensionElementsList(businessObject, 'activiti:Field'),
            add() {
                const fieldInjection = createElement('activiti:Field', {}, listener, bpmnFactory);
                addExtensionElements(element, businessObject, fieldInjection, bpmnFactory, commandStack);
            },
            remove( field ) {
                removeExtensionElements(element, businessObject, field, commandStack);
            }
        }
    }else{
        return {
            fields:listener.get('fields'),
            add() {
                const field = createElement('activiti:Field', {}, listener, bpmnFactory);
                commandStack.execute('element.updateModdleProperties', {
                    element,
                    moddleElement: listener,
                    properties: {
                        fields: [...listener.get('fields'), field]
                    }
                });
            },  remove( field) {
                commandStack.execute('element.updateModdleProperties', {
                    element,
                    moddleElement: listener,
                    properties: {
                        fields: without(listener.get('fields'), field)
                    }
                });
            }
        }
    }
}
function compareName(field, anotherField) {
    const [name = '', anotherName = ''] = [field.name, anotherField.name];
    return name === anotherName ? 0 : name > anotherName ? 1 : -1;
}
function Field(props) {
    const {
        element,
        id,
        index,
        item: field,
        open,
        register
    } = props;
    const fieldId = `${id}-field-${index}`;
    return jsx(CollapsibleEntry, {
        id: fieldId,
        element: element,
        entries: FieldInjection({
            element,
            field,
            register,
            idPrefix: fieldId
        }),
        label: field.get('name') || '<empty>',
        open: open
    });
}

function FieldInjection(props) {
    const {
        element,
        idPrefix,
        field,
        register,
        translate
    } = props;
    let arrs = ['propertyName','propertyType','propertyValue'];
    const list = []
    for (let i = 0; i < arrs.length; i++) {
        let entries = register.findEntries(arrs[i],translate)
        entries = {...entries}
        entries.id = idPrefix+"_"+arrs[i];
        entries.element =element;
        entries.field =field;
        entries.idPrefix =idPrefix;
        entries.property=field
        list.push(entries)
    }
    return list;
}
