import {SelectEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject, is} from "bpmn-js/lib/util/ModelUtil";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";
import {isAny} from "bpmn-js/lib/features/modeling/util/ModelingUtil";

const component = {
    id: 'listenerCustomType',
    component: ListenerCustomType
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function ListenerCustomType(props) {
    const { element, id,listener } = props;
    const modeling = useService('modeling');
    const translate = useService('translate');
    const bpmnFactory = useService('bpmnFactory');
    function getValue() {
        return getListenerType(listener);
    }
    function setValue(value) {
        const properties = getDefaultImplementationProperties(value, bpmnFactory);
        modeling.updateModdleProperties(element, listener, properties);
    }
    function getOptions() {
        return getListenerTypeOptions(translate);
    }
    return jsx(SelectEntry, {
        id: id,
        label: translate('ListenerCustomType'),
        getValue: getValue,
        setValue: setValue,
        getOptions: getOptions
    });
}
const CLASS_PROPS = {
    'customPropertiesResolverClass': undefined
};
const EXPRESSION_PROPS = {
    'customPropertiesResolverExpression': undefined
};
const DELEGATE_EXPRESSION_PROPS = {
    'customPropertiesResolverDelegateExpression': undefined
};
const DEFAULT_PROPS = {
    ...CLASS_PROPS,
    ...EXPRESSION_PROPS,
    ...DELEGATE_EXPRESSION_PROPS
};
function getDefaultImplementationProperties(type, bpmnFactory) {
    switch (type) {
        case 'class':
            return {
                ...DEFAULT_PROPS,
                'customPropertiesResolverClass': ''
            };
        case 'expression':
            return {
                ...DEFAULT_PROPS,
                'customPropertiesResolverExpression': ''
            };
        case 'delegateExpression':
            return {
                ...DEFAULT_PROPS,
                'customPropertiesResolverDelegateExpression': ''
            };
    }
}
const IMPLEMENTATION_TYPE_TO_LABEL = {
    class: 'Java class',
    expression: 'Expression',
    delegateExpression: 'Delegate expression'
    // ,
    // throwEvent: 'Throw Event'
};
function getListenerTypeOptions(translate) {
    return Object.entries(IMPLEMENTATION_TYPE_TO_LABEL).map(([value, label]) => ({
        value,
        label: translate(label)
    }));
}
function getListenerType(listener) {
    return getImplementationType(listener);
}
function getListenerBusinessObject(businessObject) {
    if (isAny(businessObject, ['activiti:ExecutionListener', 'activiti:TaskListener'])) {
        return businessObject;
    }
}
function getImplementationType(element) {
    const businessObject = getListenerBusinessObject(element) || getServiceTaskLikeBusinessObject(element);
    if (!businessObject) {
        return;
    }
    const cls = businessObject.get('activiti:customPropertiesResolverClass');
    if (typeof cls !== 'undefined') {
        return 'class';
    }
    const expression = businessObject.get('activiti:customPropertiesResolverExpression');
    if (typeof expression !== 'undefined') {
        return 'expression';
    }
    const delegateExpression = businessObject.get('activiti:customPropertiesResolverDelegateExpression');
    if (typeof delegateExpression !== 'undefined') {
        return 'delegateExpression';
    }
    const script = businessObject.get('throwEvent');
    if (typeof throwEvent !== 'undefined') {
        return 'throwEvent';
    }
}

function getServiceTaskLikeBusinessObject(element) {
    if (is(element, 'bpmn:IntermediateThrowEvent') || is(element, 'bpmn:EndEvent')) {
        const messageEventDefinition = getMessageEventDefinition$1(element);
        if (messageEventDefinition) {
            element = messageEventDefinition;
        }
    }
    return getBusinessObject(element);
}
function getMessageEventDefinition$1(element) {
    if (is(element, 'bpmn:ReceiveTask')) {
        return getBusinessObject(element);
    }
    return getEventDefinition$1(element, 'bpmn:MessageEventDefinition');
}
function getEventDefinition$1(element, eventType) {
    const businessObject = getBusinessObject(element);
    const eventDefinitions = businessObject.get('eventDefinitions') || [];
    return find(eventDefinitions, function (definition) {
        return is(definition, eventType);
    });
}
