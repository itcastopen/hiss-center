import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
    id: 'listenerExpression',
    component: ListenerExpression,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function ListenerExpression(props) {
    const {
        element,
        id
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const {businessObject} = props;
    const getValue = () => {
        return businessObject.get('activiti:expression');
    };
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: businessObject,
            properties: {
                'activiti:expression': value
            }
        });
    };
    const validate = value => {
        if(!value){
            return translate('value required')
        }
    }
    return TextFieldEntry({
        element,
        id,
        label: translate('Expression'),
        getValue,
        setValue,
        debounce,
        validate
    });
}
