import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
    id: 'listenerId',
    component: ListenerId,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function ListenerId(props) {
    const { element, id,listener } = props;
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const commandStack = useService('commandStack');
    let options = {
        element,
        id: id,
        label: translate('Listener ID'),
        debounce,
        isEdited: isTextFieldEntryEdited,
        setValue: value => {
            commandStack.execute('element.updateModdleProperties', {
                element,
                moddleElement: listener,
                properties: {
                    'activiti:id': value
                }
            });
        },
        getValue: () => {
            return listener.get('activiti:id');
        }
    };
    return TextFieldEntry(options);
}
