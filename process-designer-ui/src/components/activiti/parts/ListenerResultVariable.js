import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
    id: 'listenerResultVariable',
    component: ListenerResultVariable,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function ListenerResultVariable(props) {
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    const {businessObject,element} = props;

    const getValue = () => {
        return businessObject.get('activiti:resultVariable');
    };
    const setValue = value => {
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: businessObject,
            properties: {
                'activiti:resultVariable': value
            }
        });
    };
    const validate = value => {
        if(!value){
            return translate('value required')
        }
    }
    return TextFieldEntry({
        element,
        id: 'expressionResultVariable',
        label: translate('Result variable'),
        getValue,
        setValue,
        debounce,
        validate
    });
}
