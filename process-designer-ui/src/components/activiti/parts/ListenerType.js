import {SelectEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject, is} from "bpmn-js/lib/util/ModelUtil";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";
import {isAny} from "bpmn-js/lib/features/modeling/util/ModelingUtil";

const component = {
    id: 'listenerType',
    component: ListenerType
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function ListenerType(props) {
    const { element, id,listener } = props;
    const modeling = useService('modeling');
    const translate = useService('translate');
    const bpmnFactory = useService('bpmnFactory');
    function getValue() {
        return getListenerType(listener);
    }
    function setValue(value) {
        const properties = getDefaultImplementationProperties(value, bpmnFactory);
        modeling.updateModdleProperties(element, listener, properties);
    }
    function getOptions() {
        if(is(listener,'activiti:EventListener')){
            return getAllListenerTypeOptions(translate)
        }
        return get3ListenerTypeOptions(translate);
    }
    return jsx(SelectEntry, {
        id: id,
        label: translate('Listener type'),
        getValue: getValue,
        setValue: setValue,
        getOptions: getOptions
    });
}
const THROWEVENT_PROPS = {
    'throwEvent': undefined,
    'signalName': undefined,
    'messageName': undefined,
    'errorCode': undefined
};
const CLASS_PROPS = {
    'class': undefined
};
const EXPRESSION_PROPS = {
    'expression': undefined
};
const DELEGATE_EXPRESSION_PROPS = {
    'delegateExpression': undefined
};
const DEFAULT_PROPS = {
    ...THROWEVENT_PROPS,
    ...CLASS_PROPS,
    ...EXPRESSION_PROPS,
    ...DELEGATE_EXPRESSION_PROPS
};
function getDefaultImplementationProperties(type, bpmnFactory) {
    switch (type) {
        case 'class':
            return {
                ...DEFAULT_PROPS,
                'class': ''
            };
        case 'expression':
            return {
                ...DEFAULT_PROPS,
                'expression': ''
            };
        case 'delegateExpression':
            return {
                ...DEFAULT_PROPS,
                'delegateExpression': ''
            };
        case 'throwEvent':
            return {
                ...DEFAULT_PROPS,
                'throwEvent': ''
            };
    }
}
const IMPLEMENTATION_TYPE_TO_LABEL_3 = {
    class: 'Java class',
    expression: 'Expression',
    delegateExpression: 'Delegate expression'
};
const IMPLEMENTATION_TYPE_TO_LABEL_ALL = {
    class: 'Java class',
    delegateExpression: 'Delegate expression',
    throwEvent: 'Throw Event'
};
function getAllListenerTypeOptions(translate) {
    return Object.entries(IMPLEMENTATION_TYPE_TO_LABEL_ALL).map(([value, label]) => ({
        value,
        label: translate(label)
    }));
}
function get3ListenerTypeOptions(translate) {
    return Object.entries(IMPLEMENTATION_TYPE_TO_LABEL_3).map(([value, label]) => ({
        value,
        label: translate(label)
    }));
}
function getListenerType(listener) {
    return getImplementationType(listener);
}
function getListenerBusinessObject(businessObject) {
    if (isAny(businessObject, ['activiti:ExecutionListener', 'activiti:TaskListener', 'activiti:EventListener'])) {
        return businessObject;
    }
}
function getImplementationType(element) {
    const businessObject = getListenerBusinessObject(element) || getServiceTaskLikeBusinessObject(element);
    if (!businessObject) {
        return;
    }
    const cls = businessObject.get('activiti:class');
    if (typeof cls !== 'undefined') {
        return 'class';
    }
    const expression = businessObject.get('activiti:expression');
    if (typeof expression !== 'undefined') {
        return 'expression';
    }
    const delegateExpression = businessObject.get('activiti:delegateExpression');
    if (typeof delegateExpression !== 'undefined') {
        return 'delegateExpression';
    }
    const script = businessObject.get('throwEvent');
    if (typeof throwEvent !== 'undefined') {
        return 'throwEvent';
    }
}

function getServiceTaskLikeBusinessObject(element) {
    if (is(element, 'bpmn:IntermediateThrowEvent') || is(element, 'bpmn:EndEvent')) {
        const messageEventDefinition = getMessageEventDefinition$1(element);
        if (messageEventDefinition) {
            element = messageEventDefinition;
        }
    }
    return getBusinessObject(element);
}
function getMessageEventDefinition$1(element) {
    if (is(element, 'bpmn:ReceiveTask')) {
        return getBusinessObject(element);
    }
    return getEventDefinition$1(element, 'bpmn:MessageEventDefinition');
}
function getEventDefinition$1(element, eventType) {
    const businessObject = getBusinessObject(element);
    const eventDefinitions = businessObject.get('eventDefinitions') || [];
    return find(eventDefinitions, function (definition) {
        return is(definition, eventType);
    });
}
