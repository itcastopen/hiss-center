import {FieldProps} from "@/components/activiti/util";

const component = {
  id: 'mailHtml',
  component: FieldProps,
  filedName:'html',
  filedType:'expression'
}


if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

