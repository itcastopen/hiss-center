import {FieldProps} from "@/components/activiti/util";

const component = {
  id: 'mailTo',
  component: FieldProps,
  filedName:'to',
  filedType:'string'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}
