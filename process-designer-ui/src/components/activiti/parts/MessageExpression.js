import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'messageExpression',
  component: MessageExpression,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function MessageExpression(props) {
  const {
    element,
    id = 'messageExpression'
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  let businessObject = getBusinessObject(element);
  if(businessObject.eventDefinitions){
    businessObject = businessObject.eventDefinitions[0];
  }
  const getValue = () => {
    return businessObject.get('activiti:messageExpression');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'activiti:messageExpression': value || ''
      }
    });
  };
  return TextFieldEntry({
    element,
    id:id+"-messageExpression",
    label: translate('MessageExpression'),
    getValue,
    setValue,
    debounce
  });
}
