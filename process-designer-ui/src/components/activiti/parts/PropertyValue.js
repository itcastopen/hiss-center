import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
    id: 'propertyValue',
    component: PropertyValue,
    isEdited: isTextFieldEntryEdited
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function PropertyValue(props) {
    let {
        idPrefix,
        element,
        property,
        field
    } = props;
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    const debounce = useService('debounceInput');
    let type = 'value'
    if(!field){
        field = property
    }else{
        type = determineType(field);
    }
    const setValue = value => {
        // (2) set property accordingly
        const properties = Object.assign({}, DEFAULT_PROPS$2);
        properties[type] = value || '';

        // (3) execute the update command
        commandStack.execute('element.updateModdleProperties', {
            element,
            moddleElement: field,
            properties
        });
    };
    const getValue = () => {
        if(field) {
            return field.string || field.stringValue || field.expression;
        }
        return ''
    };
    return TextFieldEntry({
        element: field,
        id: idPrefix + '-value',
        label: translate('Value'),
        getValue,
        setValue,
        debounce
    });
}
const DEFAULT_PROPS$2 = {
    'stringValue': undefined,
    'string': undefined,
    'expression': undefined
};
function determineType(field) {
    // string is the default type
    return 'string' in field && 'string' || 'expression' in field && 'expression' || 'stringValue' in field && 'string' || 'string';
}
