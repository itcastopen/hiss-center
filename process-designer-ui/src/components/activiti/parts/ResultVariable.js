import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
  id: 'resultVariable',
  component: ResultVariable,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function ResultVariable(props) {
  const { element, id } = props;

  const modeling = useService('modeling');
  const translate = useService('translate');
  const debounce = useService('debounceInput');

  const getValue = () => {
    return element.businessObject.ResultVariable || '';
  }

  const setValue = value => {
    return modeling.updateProperties(element, {
      ResultVariable: value
    });
  }

  return html`<${TextFieldEntry}
    id=${ id }
    element=${ element }
    description=${ translate('If left blank, the result will not be stored by default') }
    label=${ translate('ResultVariableName') }
    getValue=${ getValue }
    setValue=${ setValue }
    debounce=${ debounce }
  />`
}
