import {
    CheckboxEntry,
    isCheckboxEntryEdited,
    isTextFieldEntryEdited,
    SelectEntry,
    TextFieldEntry
} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject, is} from "bpmn-js/lib/util/ModelUtil";
import {updateMultiInstance} from "@/components/activiti/util";

const component = {
    id: 'sequential',
    component: Sequential
}

if(window['register']) {
    window['register'].regComponent(component.id,component)
}
export default function(element) {
    component['element']=element;
    return component;
}

function Sequential(props) {
    const {
        element
    } = props;
    const modeling = useService('modeling');
    const bpmnFactory = useService('bpmnFactory');
    const commandStack = useService('commandStack');
    const translate = useService('translate');
    let getValue, setValue;
    const businessObject = getBusinessObject(element);
    let loopCharacteristics = businessObject.get('loopCharacteristics');
    getValue = () => {
        let bool = false
        if(loopCharacteristics){
            bool= loopCharacteristics.isSequential || false;
        }
        return bool;
    }
    setValue = value => {
        return updateMultiInstance(element,businessObject,bpmnFactory,commandStack,{
            isSequential: value
        })
    };
    return CheckboxEntry({
        element,
        id: element.id+'-isSequential',
        label: translate('Sequential'),
        getValue,
        setValue
    });
}
