import {isTextFieldEntryEdited, SelectEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";

const component = {
  id: 'serviceTaskType',
  component: ServiceTaskType,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function ServiceTaskType(props) {
  const { element, id } = props;
  const translate = useService('translate');
  const bpmnFactory = useService('bpmnFactory');
  const commandStack = useService('commandStack');
  const target = getBusinessObject(element)
  function getValue() {
    let expression = target.get('activiti:expression');
    if(expression){
      return 'expression';
    }
    let delegateExpression = target.get('activiti:delegateExpression');
    if(delegateExpression){
      return 'delegateExpression';
    }
    let cla = target.get('activiti:class');
    if(cla){
      return 'class';
    }
  }
  function setValue(value) {
    target.type=value
    let temp = {'activiti:class':undefined,'activiti:expression':undefined,'activiti:delegateExpression':undefined,type:undefined}
    temp['activiti:'+value]=''
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: target,
      properties:temp
    });
  }
  function getOptions() {
      return [{
        value: 'class',
        label: translate('javaClass')
      }, {
        value: 'expression',
        label: translate('expression')
      }, {
        value: 'delegateExpression',
        label: translate('delegateExpression')
      }
      // , {
      //   value: 'mail',
      //   label: translate('mail')
      // },
      //   {
      //   value: 'mule',
      //   label: translate('mule')
      // }, {
      //   value: 'camel',
      //   label: translate('camel')
      // },
      //   {
      //   value: 'shell',
      //   label: translate('shell')
      // }
      ];
  }
  return jsx(SelectEntry, {
    id: id+'-serviceTaskType',
    label: translate('ServiceTask type'),
    getValue: getValue,
    setValue: setValue,
    getOptions: getOptions
  });
}
