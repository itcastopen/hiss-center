import {html} from 'htm/preact';
import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
  id: 'initiator',
  component: Initiator,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function Initiator(props) {
  const { element, id } = props;

  const modeling = useService('modeling');
  const translate = useService('translate');
  const debounce = useService('debounceInput');

  const getValue = () => {
    return element.businessObject.initiator || '';
  }

  const setValue = value => {
    return modeling.updateProperties(element, {
      initiator: value
    });
  }
  const validate = value => {
    if(!value){
      return translate('value required')
    }
  }

  return html`<${TextFieldEntry}
    id=${ id }
    element=${ element }
    description=${ translate('By using this name, the initiator can be obtained in the process') }
    label=${ translate('Initiator') }
    getValue=${ getValue }
    setValue=${ setValue }
    debounce=${ debounce }
    validate=${ validate }
  />`
}
