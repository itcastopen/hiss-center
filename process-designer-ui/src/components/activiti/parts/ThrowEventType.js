import {SelectEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";

const component = {
  id: 'throwEventType',
  component: ThrowEventType
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function ThrowEventType(props) {
  const { element, id,listener } = props;
  const translate = useService('translate');
  const bpmnFactory = useService('bpmnFactory');
  const commandStack = useService('commandStack');
  function getValue() {
    return listener.get('event');
  }
  function setValue(value) {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: listener,
      properties:{
        "throwEvent":value
      }
    });
  }
  function getOptions() {
    return [{
      value: 'signal',
      label: translate('signal')
    }, {
      value: 'globalSignal',
      label: translate('globalSignal')
    }, {
      value: 'message',
      label: translate('message')
    }, {
      value: 'error',
      label: translate('error')
    }];
  }
  return jsx(SelectEntry, {
    id: id+"-ThrowEventType",
    label: translate('ThrowEventType'),
    getValue: getValue,
    setValue: setValue,
    getOptions: getOptions
  });
}
