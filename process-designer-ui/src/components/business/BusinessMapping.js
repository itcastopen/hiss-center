// 定义每个元素的分组信息：包含的字段属性
import conditionGroup from "@/components/business/group/ConditionGroup";

const businessMapping = {
    'starter':{
        'starterPeopleGroup':[],
        'formGroup':[]
    },
    'singleApprove':{
        'singleApprovePeopleGroup':[],
        'formGroup':[]
    },
    'multipleApprove':{
        'multipleApprovePeopleGroup':[],
        'formGroup':[]
    },
    'notifier':{
        'informedPeopleGroup':[],
        'formGroup':[]
    },
    'cc':{
        'ccPeopleGroup':[],
        'formGroup':[]
    },
    'condition':{
        'conditionGroup':[]
    }
}

export default businessMapping;
