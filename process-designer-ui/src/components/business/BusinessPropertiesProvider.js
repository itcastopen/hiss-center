import register from './BusinessRegister';

const LOW_PRIORITY = 500;

export default function BusinessPropertiesProvider(propertiesPanel, translate, injector) {
  // 显示当前元素的属性组
  this.getGroups = function(element) {

    // 在原油groups基础上，判断元素是否新增组
    return function(groups) {
      return register.findGroup(groups,element, translate,injector)
    }
  };
  // 注册到属性面板中，属性优先级低，则排列在基础属性后面
  propertiesPanel.registerProvider(LOW_PRIORITY, this);
}

BusinessPropertiesProvider.$inject = [ 'propertiesPanel', 'translate','injector' ];
