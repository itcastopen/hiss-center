const register = new BusinessRegister();
window['register'] = register;
import activitiMapping from './BusinessMapping'
// 导入字段组件
require('./parts/StarterPeopleType')
require('./parts/StarterFixedPeople')
require('./parts/FormType')
require('./parts/FormId')
require('./parts/FormFieldAuth')
require('./parts/FormFieldList')
// ================= 单节点审批 ==================
require('./parts/SingleApproveUserType')
require('./parts/SingleApproveModeType')
require('./parts/SingleApproveDesignatedList')
// ================= 多节点审批 ==================
require('./parts/MultipleApproveOrderType')
// ================= 条件节点审批 ==================
require('./parts/ConditionType')
require('./parts/ConditionFieldList')
require('./parts/ConditionFieldType')
require('./parts/ConditionFieldValue1')
require('./parts/ConditionFieldValue2')
// ================= 审批按钮 ==================
require('./parts/PassFunction')
require('./parts/PassFunctionText')
require('./parts/UnPassFunction')
require('./parts/UnPassFunctionText')
require('./parts/TemporaryFunction')
require('./parts/TemporaryFunctionText')
require('./parts/WithdrawFunction')
require('./parts/WithdrawFunctionText')
require('./parts/CcFunction')
require('./parts/CcFunctionText')
require('./parts/DelegateFunction')
require('./parts/DelegateFunctionText')
require('./parts/InformFunction')
require('./parts/InformFunctionText')
require('./parts/ReturnFunction')
require('./parts/ReturnFunctionText')
require('./parts/AddSignatureFunction')
require('./parts/AddSignatureFunctionText')
require('./parts/RubutFunction')
require('./parts/RubutFunctionText')

function BusinessRegister(){
    this.components= {};
    this.groups = {};
    this.regComponent = function (id, component) {
        // console.log("===============================regComponent=:"+id)
        this.components[id] = component;
    };
    this.regGroup = function (module) {
        let group = module.default
        // console.log("===============================regGroup=:"+group.id)
        this.groups[group.id] = group;
    };
   this.findGroup = function (groups,element, translate,injector){
       var type = element.hissType;
       if(element.businessObject){
           type = element.businessObject.get('hissType')
       }
       // console.log("===============================findGroup=:"+type)
       // console.log(element)
       var groupMapping = activitiMapping[type];
       groups = []
       if(groupMapping) {
            for(let key in groupMapping){
                let groupFields = groupMapping[key];
                let entries = [];
                for (let i = 0; i < groupFields.length; i++) {
                    let field = this.findEntries(groupFields[i],translate);
                    // console.log(groupFields[i]+"===============================field:"+field)
                    if(field) {
                        entries.push(field)
                    }
                }
                let groupComponent = this.groups[key];
                groupComponent.label = translate(groupComponent.label)
                groupComponent.entries = entries;
                if(groupComponent.function) {
                    let atts = groupComponent.function.call(this,groups,element, translate,injector,this);
                    groupComponent = {...groupComponent,...atts}
                }
                groups.push(groupComponent)
            }
       }
       // console.log("===============================group:"+groups)
       // console.log(groups)
       return groups;
   };
    this.findEntries = function (id, translate){
        // console.log(this.components)
        // console.log(id+"================this.components===============:"+this.components)
        return this.components[id];
    }
}

export default register

// 导入注册器
const starterPeopleGroup = require('./group/StarterPeopleGroup')
register.regGroup(starterPeopleGroup)
const formGroup = require('./group/FormGroup')
register.regGroup(formGroup)
const singleApprovePeopleGroup = require('./group/SingleApprovePeopleGroup')
register.regGroup(singleApprovePeopleGroup)
const multipleApprovePeopleGroup = require('./group/MultipleApprovePeopleGroup')
register.regGroup(multipleApprovePeopleGroup)
const informedPeopleGroup = require('./group/InformedPeopleGroup')
register.regGroup(informedPeopleGroup)
const ccPeopleGroup = require('./group/CcPeopleGroup')
register.regGroup(ccPeopleGroup)
const conditionGroup = require('./group/ConditionGroup')
register.regGroup(conditionGroup)
