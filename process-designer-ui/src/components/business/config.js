import Constants from "@/components/business/constants";

const CUSTOM_TYPE_CONFIG = {
    'starter':{
        name:'发起人',
        bgColor:'#509eff',
        title:'创建发起人节点',
        defaultFormAuth:'edit',
        bindTitleField : Constants.hissBusinessStarterUserType
    },
    'singleApprove':{
        name:'单人办理',
        bgColor:'#ff943e',
        title:'创建单人办理节点',
        defaultFormAuth:'view',
        bindTitleField : Constants.hissBusinessSingleMode
    },
    'multipleApprove':{
        name:'多人办理',
        bgColor:'#ff943e',
        title:'创建多人办理节点',
        defaultFormAuth:'view',
        bindTitleField : Constants.hissBusinessSingleMode
    },
    'notifier':{
        name:'知会人',
        bgColor:'#f56c6c',
        title:'创建知会人办理节点',
        defaultFormAuth:'view',
        bindTitleField : Constants.hissBusinessSingleMode
    },
    'condition':{
        name:'条件',
        bgColor:'#52c1f5',
        title:'创建条件办理节点',
        defaultFormAuth:'view',
        bindTitleField : Constants.hissBusinessConditionType
    },
    'cc':{
        name:'抄送人',
        bgColor:'#576a95',
        title:'创建抄送人节点',
        defaultFormAuth:'view',
        bindTitleField : Constants.hissBusinessSingleMode
    }
}
export default CUSTOM_TYPE_CONFIG
