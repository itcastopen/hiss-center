const CUSTORM_FIELDS = {
    'xxxFunction':'xxxFunction',//是否显示的按钮
    'xxxFunctionText':'xxxFunctionText',//按钮上显示的文字
    'hissBusinessStarterUserType': 'hissBusinessStarterUserType',//'发起人的类型',
    'hissBusinessStarterFixedUser': 'hissBusinessStarterFixedUser',//'固定的发起人或表达式',
    'hissBusinessFormType':'hissBusinessFormType',// 表单类型，内外部的
    // =================== 单节点审批 ========================
    'hissBusinessSingleMode':'hissBusinessSingleMode',// 办理类型，人工办理、自动通过、自动拒绝
    /**
     * 办理人类型：
     * 1、直属领导
     * 2、直属部门领导
     * 3、指定人员
     * 4、指定角色
     * 5、指定部门
     * 6、发起人自选
     * 7、发起人自己
     */
    'hissBusinessSingleUserType':'hissBusinessSingleUserType',
    'hissBusinessSingleUserList':'hissBusinessSingleUserList',// 指定人员、角色、部门、自选范围的存储值
    // =================== 单节点审批 ========================
    'multipleApproveOrderType':'multipleApproveOrderType', // 多人审批模式
    // =================== 条件节点 ========================
    'hissBusinessConditionType':'hissBusinessConditionType', // 条件类型，默认条件，普通条件


    values:{
        innerForm:'innerForm', //'内部表单，也就是流程引擎自己的的
        externalForm:'externalForm',//'外部表单，业务系统自己的
        fixedUser:'fixedUser', //'发起人固定用户类型值',
        currentLoginUser:'currentLoginUser',//'发起人是当前登录人',
        formFieldAuthView:'view',//表单可查看权限
        formFieldAuthEdit:'edit',//表单可编辑权限
        formFieldAuthHide:'hide',//表单隐藏权限
        // =================== 单节点审批 ========================
        singleModeManual:'manual',// 人工
        singleModeAutoApproval:'approval',// 自动通过
        singleModeAutoRejection:'rejection',// 自动拒绝
        singleUserTypeDirectLeader:'directLeader',// 直属领导
        singleUserTypeDirectDepartment:'directDepartment',// 直属部门领导
        singleUserTypeDesignatedUser:'designatedUser',// 指定用户
        singleUserTypeDesignatedRole:'designatedRole',// 指定角色
        singleUserTypeDesignatedDepartment:'designatedDepartment',// 指定部分
        singleUserTypeOwnerSelect:'ownerSelect',// 发起人自选
        singleUserTypeOwner:'owner',// 发起人自选
        // =================== 多人节点审批 ========================
        multipleOrderSerial:'addSign', //串行审批，串签
        multipleOrderCountersign:'counterSign', //并行审批，会签
        multipleOrderOr1:'orSign1', //并行或签，1人
        multipleOrderOr2:'orSign2', //并行或签，2人
        multipleOrderOr3:'orSign3', //并行或签，3人
        multipleOrderOrPer5:'orSignPer5', //并行或签，>50%的人办理即可
        // =================== 条件节点 ========================
        defaultCondition:'defaultCondition',// 默认条件
        plainCondition:'plainCondition',// 普通
        gt:'gt',
        gte:'gte',
        lt:'lt',
        lte:'lte',
        eq:'eq',
        between:'between',
        in:'in',
        notIn:'notIn',
    }
}
export default CUSTORM_FIELDS
