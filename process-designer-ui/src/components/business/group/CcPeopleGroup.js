import {ListGroup} from "@bpmn-io/properties-panel";
import {BusinessPeopleGroupProps} from "@/components/business/util";

const informedPeopleGroup = {
    id: 'ccPeopleGroup',
    label: 'CcPeopleGroup',
    shouldOpen:true,
    component:ListGroup,
    function:function (groups,element, translate,injector,register){
        element.translate = translate
        let a = BusinessPeopleGroupProps({
            element,
            translate,
            injector,
            register,
            buttons:['ccFunction','informFunction'],
            types:['singleApproveModeType','singleApproveUserType']
        })
        return a;
    }
};
export default informedPeopleGroup;
