import {ListGroup} from "@bpmn-io/properties-panel";
import {getBusinessObject, is} from "bpmn-js/lib/util/ModelUtil";
import {BusinessPeopleGroupProps, getExtensionElementsAndName} from "@/components/business/util";
import Constants from "@/components/business/constants";

const singleApprovePeopleGroup = {
    id: 'singleApprovePeopleGroup',
    label: 'SingleApprovePeopleGroup',
    shouldOpen:true,
    component:ListGroup,
    function:function (groups,element, translate,injector,register){
        element.translate = translate
        let a = BusinessPeopleGroupProps({
            element,
            translate,
            injector,
            register,
            buttons:['passFunction','unPassFunction','rubutFunction','returnFunction','delegateFunction','withdrawFunction','temporaryFunction','ccFunction','informFunction'],
            types:['singleApproveModeType','singleApproveUserType']
        })
        return a;
    }
};
export default singleApprovePeopleGroup;
