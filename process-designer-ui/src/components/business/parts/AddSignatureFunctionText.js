import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'addSignatureFunctionText',
  component: FieldProps,
  filedName:'addSignatureFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'addSignatureFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}
