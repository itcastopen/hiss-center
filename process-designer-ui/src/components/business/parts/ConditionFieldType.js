import {FieldCommonSelect} from "@/components/business/util";
import Constants from "@/components/business/constants";
import {useService} from "bpmn-js-properties-panel";
import {SelectEntry, TextFieldEntry} from "@bpmn-io/properties-panel";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";

const component = {
  id: 'conditionFieldType',
  component: FieldCommonSelect,
  filedType:'string',
  defaultValue:Constants.values.eq,
  getOptions :function(translate) {
    return [{
      value: Constants.values.gt,
      label: translate(Constants.values.gt)
    }, {
      value: Constants.values.gte,
      label: translate(Constants.values.gte)
    }, {
      value: Constants.values.lt,
      label: translate(Constants.values.lt)
    }, {
      value: Constants.values.lte,
      label: translate(Constants.values.lte)
    }, {
      value: Constants.values.eq,
      label: translate(Constants.values.eq)
    }, {
      value: Constants.values.between,
      label: translate(Constants.values.between)
    }, {
      value: Constants.values.in,
      label: translate(Constants.values.in)
    }, {
      value: Constants.values.notIn,
      label: translate(Constants.values.notIn)
    }];
  }
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}
