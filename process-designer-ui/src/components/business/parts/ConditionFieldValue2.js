import {FieldProps} from "@/components/business/util";
import Constants from "@/components/business/constants";

const component = {
  id: 'conditionFieldValue2',
  component: FieldProps,
  filedType:'string',
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}
