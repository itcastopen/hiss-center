import {FieldCommonSelect} from "@/components/business/util";
import Constants from "@/components/business/constants";
import {useService} from "bpmn-js-properties-panel";
import {SelectEntry, TextFieldEntry} from "@bpmn-io/properties-panel";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";

const component = {
  id: 'conditionType',
  component: FieldCommonSelect,
  filedName:Constants.hissBusinessConditionType,
  filedType:'string',
  defaultValue:Constants.values.defaultCondition,
  getOptions :function(translate) {
    return [{
      value: Constants.values.defaultCondition,
      label: translate(Constants.values.defaultCondition)
    }, {
      value: Constants.values.plainCondition,
      label: translate(Constants.values.plainCondition)
    }];
  }
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}
