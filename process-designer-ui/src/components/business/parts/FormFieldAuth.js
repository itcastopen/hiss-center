import {
  addExtensionElements,
  createElement,
  FieldCommonSelect,
  getExtensionElementsAndName, removeExtensionElements
} from "@/components/business/util";
import Constants from "@/components/business/constants";
import {useService} from "bpmn-js-properties-panel";
import {RadioEntry} from "@bpmn-io/properties-panel";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";
import {getBusinessObject} from "bpmn-js/lib/util/ModelUtil";

const component = {
  id: 'formFieldAuth',
  component: FormPropertyAuth,
  filedType:'string',
  shouldOpen:true,
  getOptions :function(translate) {
    return [{
      value: Constants.values.formFieldAuthEdit,
      label: translate(Constants.values.formFieldAuthEdit)
    },{
      value: Constants.values.formFieldAuthView,
      label: translate(Constants.values.formFieldAuthView)
    }, {
      value: Constants.values.formFieldAuthHide,
      label: translate(Constants.values.formFieldAuthHide)
    }];
  }
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}

export function FormPropertyAuth(props) {
  const { element, id, filedName, filedType,getOptions,field,formData,defaultFormAuthValue} = props;
  const commandStack = useService('commandStack');
  const bpmnFactory = useService('bpmnFactory');
  const modeling = useService('modeling');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const businessObject = getBusinessObject(element)
  let extensionElementsTo = getExtensionElementsAndName(businessObject,'activiti:FormProperty',filedName);
  let fieldElement;
  if(extensionElementsTo&&extensionElementsTo.length){
    for (let i = 0; i < extensionElementsTo.length; i++) {
      if(formData.id==extensionElementsTo[i].expression){
        fieldElement = extensionElementsTo[i]
      }
    }
  }
  const getValue = () => {
    if(fieldElement) {
      let value = fieldElement.type||'';
      return value
    }
    return '';
  }

  const setValue = value => {
    value = value.srcElement.value
    // 如果值不为空，元素不存在，创建
    if(value&&!fieldElement){
      fieldElement = createElement('activiti:FormProperty', {
        name:filedName,
        expression:formData.id
      }, businessObject, bpmnFactory);
      addExtensionElements(element, businessObject, fieldElement, bpmnFactory, commandStack);
    }
    // 有值则更新
    if(value){
      let properties = {};
      properties['type'] = value
      return commandStack.execute('element.updateModdleProperties', {
        element,
        moddleElement: fieldElement,
        properties
      });
    }else {
      // 删除元素
      removeExtensionElements(element, businessObject, fieldElement, commandStack);
    }
  }
  const getOptionsFun = function (){
    return getOptions(translate)
  }
  return jsx(RadioEntry, {
    id: id,
    label: '',
    defaultValue:defaultFormAuthValue,
    getValue: getValue,
    setValue: setValue,
    getOptions: getOptionsFun
  });
}
