import {FieldCommonSelect} from "@/components/business/util";
import Constants from "@/components/business/constants";
import {useService} from "bpmn-js-properties-panel";
import {SelectEntry, TextFieldEntry} from "@bpmn-io/properties-panel";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";

const component = {
  id: 'formId',
  component: FormId,
  filedName:Constants.hissBusinessFormType,
  filedType:'string',
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}

function FormId(props) {
  let {
    idPrefix,
    element,
    property
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  let type = 'value'
  const setValue = value => {
    const properties = Object.assign({}, DEFAULT_PROPS$2);
    let json = property.get('value')
    if(!json){
      json = {};
    }else{
      json = eval('('+json+')')
    }
    json['id']=value
    json['node']=element.id
    value = JSON.stringify(json);
    properties[type] = value || '';
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: property,
      properties
    });
  };
  const getValue = () => {
    if(property) {
      let value = property.get('value');
      if(value){
        let temp = eval('('+value+')')
        return temp['id']||'';
      }
    }
    return ''
  };
  return TextFieldEntry({
    id: idPrefix+"-id",
    label: translate('FormId'),
    getValue: getValue,
    setValue: setValue,
    debounce
  });
}
const DEFAULT_PROPS$2 = {
  'stringValue': undefined,
  'string': undefined,
  'expression': undefined
};
function determineType(field) {
  // string is the default type
  return 'string' in field && 'string' || 'expression' in field && 'expression' || 'stringValue' in field && 'string' || 'string';
}
