import {FieldCommonSelect} from "@/components/business/util";
import Constants from "@/components/business/constants";

const component = {
  id: 'multipleApproveOrderType',
  component: FieldCommonSelect,
  filedName:Constants.multipleApproveOrderType,
  filedType:'string',
  defaultValue:Constants.values.multipleOrderSerial,
  getOptions :function(translate) {
    return [{
      value: Constants.values.multipleOrderSerial,
      label: translate(Constants.values.multipleOrderSerial)
    }, {
      value: Constants.values.multipleOrderCountersign,
      label: translate(Constants.values.multipleOrderCountersign)
    }, {
      value: Constants.values.multipleOrderOr1,
      label: translate(Constants.values.multipleOrderOr1)
    }, {
      value: Constants.values.multipleOrderOr2,
      label: translate(Constants.values.multipleOrderOr2)
    }, {
      value: Constants.values.multipleOrderOr3,
      label: translate(Constants.values.multipleOrderOr3)
    }, {
      value: Constants.values.multipleOrderOrPer5,
      label: translate(Constants.values.multipleOrderOrPer5)
    }];
  }
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}

