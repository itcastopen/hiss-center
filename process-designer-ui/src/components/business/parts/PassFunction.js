import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'passFunction',
  component: CheckboxProps,
  filedName:'passFunction',
  filedType:'string'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}
