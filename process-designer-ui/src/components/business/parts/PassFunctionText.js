import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'passFunctionText',
  component: FieldProps,
  filedName:'passFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'passFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}
