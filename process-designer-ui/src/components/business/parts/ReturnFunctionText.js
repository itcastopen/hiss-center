import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'returnFunctionText',
  component: FieldProps,
  filedName:'returnFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'returnFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}
