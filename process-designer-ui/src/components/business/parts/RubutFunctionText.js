import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'rubutFunctionText',
  component: FieldProps,
  filedName:'rubutFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'rubutFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}
