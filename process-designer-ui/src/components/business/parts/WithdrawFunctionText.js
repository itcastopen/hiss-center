import {CheckboxProps, FieldProps} from "@/components/business/util";

const component = {
  id: 'withdrawFunctionText',
  component: FieldProps,
  filedName:'withdrawFunctionText',
  filedType:'string',
  labelPostion:'right',
  placeholder:'withdrawFunctionTextPlaceholder'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}
