import {
  assign,omit
} from 'min-dash';
import PaletteProvider from 'bpmn-js/lib/features/palette/PaletteProvider'
import businessConfig from '../business/config'
import { getDi } from 'bpmn-js/lib/util/ModelUtil';

export default function DevPaletteProvider(
    palette, create, elementFactory,
    spaceTool, lassoTool, handTool,
    globalConnect, translate) {

  this._palette = palette;
  this._create = create;
  this._elementFactory = elementFactory;
  this._spaceTool = spaceTool;
  this._lassoTool = lassoTool;
  this._handTool = handTool;
  this._globalConnect = globalConnect;
  this._translate = translate;
  this._paletteProvider = new PaletteProvider(palette, create, elementFactory,
      spaceTool, lassoTool, handTool,
      globalConnect, translate);

  palette.registerProvider(this);
}

DevPaletteProvider.$inject = [
  'palette',
  'create',
  'elementFactory',
  'spaceTool',
  'lassoTool',
  'handTool',
  'globalConnect',
  'translate'
];



DevPaletteProvider.prototype.getPaletteEntries = function(element) { // 此方法和上面案例的一样
  var actions = this._paletteProvider.getPaletteEntries();
  let create = this._create
  let elementFactory = this._elementFactory
  function createListener(event) {
    var shape = elementFactory.createShape(assign({ type: 'bpmn:UserTask' }));
    create.start(event, shape);
  }
  let task = actions['create.task']
  if(task){
    task.className='bpmn-icon-user-task'
    task.action={
      dragstart: createListener,
      click: createListener
    }
  }

  return actions;
}
