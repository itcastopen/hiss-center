import CustomDevRenderer from './CustomDevRenderer';

export default {
  __init__: [ 'customRenderer' ],
  customRenderer: [ 'type', CustomDevRenderer ]
};
