import { createApp } from 'vue'
// 以下为bpmn工作流绘图工具的样式
import 'bpmn-js/dist/assets/diagram-js.css' // 左边工具栏以及编辑节点的样式
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css'
import 'bpmn-js-properties-panel/dist/assets/properties-panel.css'
// element元素
import 'element-plus/dist/index.css'
import ElementPlus from 'element-plus'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// highlightjs高亮
import 'highlight.js/styles/stackoverflow-light.css'
import hljs from 'highlight.js/lib/core';
import xml from 'highlight.js/lib/languages/xml';
import hljsVuePlugin from "@highlightjs/vue-plugin";
// 引入axios
import request from './utils/request';

import App from './App.vue'
// 路由
import routers from './router'
import {createRouter, createWebHashHistory} from "vue-router/dist/vue-router";
import routes from "./router";

hljs.registerLanguage('xml', xml);

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
const router = createRouter({
    history: createWebHashHistory(), // process.env.BASE_URL
    routes
})

app.request = request
app.use(ElementPlus)
app.use(hljsVuePlugin)
app.use(router)
window['hissProcess'] = app

export default app
app.mount('#app')
