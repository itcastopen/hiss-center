const routes = [
    {
        path: '/dev/:id',
        name: 'editDev',
        components: {
            pdu : () => import('../views/DesignDev.vue')
        },
        children: []
    },
    {
        path: '/dev',
        name: 'newDev',
        components: {
            pdu : () => import('../views/DesignDev.vue')
        },
        children: []
    },
    {
        path: '/dev-viewer/:id',
        name: 'DevViewer',
        components: {
            pdu : () => import('../views/ViewerDev.vue')
        },
        children: []
    },
    {
        path: '/bis-viewer/:id',
        name: 'BisViewer',
        components: {
            pdu : () => import('../views/ViewerBis.vue')
        },
        children: []
    },
    {
        path: '/handler-dev/:id',
        name: 'HandlerDev2',
        components:{
            pdu : () => import('../views/HandlerDev.vue')
        },
        children: []
    },
    {
        path: '/handler-bis/:id',
        name: 'HandlerBis',
        components:{
            pdu : () => import('../views/HandlerBis.vue')
        },
        children: []
    },
    {
        path: '/bis/:id',
        name: 'editBis',
        components:{
            pdu : () => import('../views/DesignBis.vue')
        },
        children: []
    },
    {
        path: '/bis',
        name: 'newBis',
        components:{
            pdu : () => import('../views/DesignBis.vue')
        },
        children: []
    },
    {
        path: '/fdl/:id',
        name: 'FormDataList',
        components:{
            pdu : () => import('../views/FormDataList.vue')
        },
        children: []
    }
]

export default routes
