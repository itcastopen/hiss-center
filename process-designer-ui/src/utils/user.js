const HEADER_TOKEN_NAME = 'hiss_token'
const AUTH_USER = 'hiss_user'
// let user
// localStorage.setItem(HEADER_TOKEN_NAME,"iamtoken")
//  user = {appId:'zhyl_app',userId:'1671403256519077891',userName:'高老师'}
// user = {appId:'zhyl_app',userId:'1671403256519078005',userName:'张三'}
// user = {appId:'zhyl_app',userId:'1',userName:'王五'}
// user = {appId:'zhyl_app',userId:'2',userName:'赵六'}
// user = {appId:'zhyl_app',userId:'1671403256519078005',userName:'李刚'}
//  user = {appId:'zhyl_app',userId:'888',userName:'小罗'}
// user = {"userId":"1671403256519078010","userName":"李刚","appId":"zhyl_app"}
// user = {"userId":"1671403256519078009","userName":"李海","appId":"zhyl_app"}
// user = {"userId":"42376301c89b238420a7365bdf4f9797","userName":"智慧养老","appId":"tenant_hiss"}
// user = {"userId":"1671403256519078069","userName":"哈哈静","appId":"zhyl_app"}
// localStorage.setItem(AUTH_USER,JSON.stringify(user))

function MessauthManager(){

}
MessauthManager.prototype.getToken = function (){
    return localStorage.getItem(HEADER_TOKEN_NAME);
}

MessauthManager.prototype.getUserId = function (){
    let messageAuth = this.getMessageAuth();
    let currentUser = messageAuth.currentUser;
    if(currentUser){
        return currentUser.userId
    }
    return undefined
}

MessauthManager.prototype.getMessageAuth = function (){
    let item = localStorage.getItem(AUTH_USER);
    if(item){
        item = JSON.parse(item)
    }else{
        item = {}
    }
    const messageAuth = { currentUser:{} }
    if(item['appId']){ // 记录当前的应用ID
        messageAuth['tenant'] = item['appId']
    }
    if(item['userId']){ // 记录当前的用户ID
        messageAuth.currentUser['userId'] = item['userId']
    }
    if(item['userName']){ // 记录当前的用户名称
        messageAuth.currentUser['userName'] = item['userName']
    }
    return messageAuth
}
// 更新当前所在的应用id
MessauthManager.prototype.updateTenant = function (appId){
    let item = localStorage.getItem(AUTH_USER);
    if(item){
        item = JSON.parse(item)
    }else{
        item = {}
    }
    item['tenant'] = appId
    localStorage.setItem(AUTH_USER,JSON.stringify(item))
}
const messauthManager = new MessauthManager();
export default messauthManager
