const { defineConfig } = require('@vue/cli-service')
const NODE_ENV = process.env.NODE_ENV
var path = require('path')
module.exports = defineConfig({
  css:{
    extract:false
  },
  productionSourceMap: true,
  transpileDependencies: true,
  outputDir: 'dist', // 打包输出目录
  configureWebpack: {
    optimization: {
      splitChunks: false,
    }
  },

  // chainWebpack: (config) => {
  //   // 清空默认的entry配置
  //   config.entryPoints.clear();
  //
  //   // 获取src/components下的所有组件目录
  //   const glob = require('glob');
  //   const components = glob.sync('./src/views/*.vue');
  //
  //   // 遍历每个组件，创建独立的entry配置
  //   components.forEach((component) => {
  //     const componentName = component.replace('\.vue','')
  //     config.entry(componentName)
  //         .add(component)
  //         .end()
  //         .default;
  //   });
  //
  //   // 设置输出目录和文件名，将[name]占位符替换为组件名称
  //   config.output
  //       .library('[name]')
  //       .filename('[name].js')
  //       .libraryTarget('umd')
  //       .umdNamedDefine(true);
  // }

})
