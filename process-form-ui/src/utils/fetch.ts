export const request = (
  url: string,
  method = 'get',
  params: Object,
  headers?: any
) => {
  return new Promise((resolve, reject) => {
    return fetch(url, {
      method,
      body: JSON.stringify(params),
      headers: {
        'Content-Type': 'application/json',
        ...headers
      }
    })
      .then((response) => {
        return response.json()
      })
      .then((res) => {
        if (res.code === 200 || res.code === '200') {
          resolve(res.data)
        } else {
          reject(res)
        }
      })
      .catch((err) => {
        reject(err)
      })
  })
}
