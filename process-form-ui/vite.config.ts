import { ConfigEnv, UserConfig, loadEnv } from 'vite'
import createVuePlugin from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import svgLoader from 'vite-svg-loader'

import path from 'path'

const CWD = process.cwd()

export default ({ mode }: ConfigEnv): UserConfig => {
  const { VITE_BASE_URL } = loadEnv(mode, CWD)
  return {
    base: './', // VITE_BASE_URL
    define: {},
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src')
      }
    },

    css: {
      preprocessorOptions: {
        less: {
          modifyVars: {
            hack: `true; @import (reference) "${path.resolve(
              'src/style/index.less'
            )}";`
          },
          math: 'strict',
          javascriptEnabled: true
        }
      }
    },

    plugins: [createVuePlugin(), vueJsx(), svgLoader()],

    server: {
      port: 3000,
      host: '0.0.0.0',
      open: false,
      hmr: true,
      proxy: {
        '/api/v1/receiver': {
          target: 'http://localhost:8081', // 测试环境的IP
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '')
        },
        '/api': {
          target: 'http://mock.boxuegu.com/mock/3547/',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '')
        }
      }
    },
    build: {
      outDir: '../process-designer-ui/public', // '/Users/zhangjian/Desktop/hiss/process-designer-ui/public/',
      rollupOptions: {
        input: './src/build.ts',
        output: {
          // chunkFileNames: 'js/[name]-[hash].js',
          assetFileNames: 'assets/[name].[ext]',
          entryFileNames: 'build.js'
        }
      }
    }
  }
}
